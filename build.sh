#!/usr/bin/env bash
source properties.sh
if [ "$0" = "$BASH_SOURCE" ] ; then
    rm -f $jarFile
    if [ -x "$(command -v mvn)" ]; then
         mvn clean install assembly:single "-DjarVersion=$jarVersion" "-DjarName=$jarName"
    else
	    echo "Maven is not installed"
	    exit 1
    fi
    cd target
    cp $jarFile ../
    cd ..
fi
