#!/usr/bin/env bash
source properties.sh
if [ "$0" = "$BASH_SOURCE" ] ; then
    rm -f $jarFile
    cd ..
    if [ -x "$(command -v mvn)" ]; then
         mvn clean install assembly:single "-DjarVersion=$jarVersion" "-DjarName=$jarName"
    else
	    echo "Maven is not installed"
	    exit 1
    fi
    cd target
    echo "Copying "$jarFile " to docker directory"
    cp $jarFile ../docker
    cd ..
fi
