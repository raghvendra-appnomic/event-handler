#!/usr/bin/env bash

source properties.sh

rm -rf $jarFile
rm -rf conf/
rm -rf *.tar
rm -rf *.tar.gz

bash ./build.sh

mkdir conf/
cp ../conf/* conf/

bash ./generateDockerFile.sh

echo "building docker image $dockerImageName:$tagName"
docker build -t $dockerImageName:${tagName} .
echo "Creating tar file"
docker save -o $dockerImageName:$tagName.tar $dockerImageName:$tagName
echo "Done dona done done"
