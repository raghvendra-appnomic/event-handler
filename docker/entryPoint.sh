#!/bin/sh
source properties.sh

if [ "x$UID" != "x" ]; then
        addgroup -g $UID appuser
        adduser -S -u $GID appuser -G appuser
fi


java -XX:+UseConcMarkSweepGC -jar -Dlogback.configurationFile="/opt/heal/event-handler/conf/logback.xml" -Djavax.net.ssl.trustStore="/opt/heal/event-handler/conf/cacerts" /opt/heal/event-handler/$jarFile -c /opt/heal/event-handler/conf/hnf-all-chains.yaml

# The infinite loop below stops the container from exitting in case of an issue during initialization... Enable in case you want to troubleshoot

#while true
#do
#	echo "Press [CTRL+C] to stop.."
#	sleep 1
#done