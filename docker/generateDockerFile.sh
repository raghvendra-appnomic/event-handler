#!/usr/bin/env bash
source properties.sh
if [ "$0" = "$BASH_SOURCE" ] ; then
    rm -f Dockerfile
    if [ -e $jarFile ]
    then
         echo "FROM openjdk:8-jre
RUN apt-get update && apt-get install -y bash vim net-tools curl

COPY $jarFile  /opt/heal/event-handler/
COPY conf/hnf-all-chains.yaml   /opt/heal/event-handler/conf/
COPY conf/heal-grpc.cert  /opt/heal/event-handler/conf/
COPY conf/cacerts  /opt/heal/event-handler/conf/
COPY entryPoint.sh /opt/heal/event-handler/
COPY source.sh /opt/heal/event-handler/
COPY logback.xml /opt/heal/event-handler/conf/
COPY conf/* /opt/heal/event-handler/samples/

RUN chmod +x /opt/heal/event-handler/entryPoint.sh
RUN mkdir /opt/heal/event-handler/logs/

ENTRYPOINT ["/opt/heal/event-handler/entryPoint.sh"]" > Dockerfile
    fi
fi
