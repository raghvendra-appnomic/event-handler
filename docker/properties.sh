#!/usr/bin/env bash

export  jarVersion=1.0.0
export  jarName=event-handler
export  jarFile=$jarName"-"$jarVersion"-jar-with-dependencies.jar"
export  dockerImageName="event-handler-img"
export  tagName=$jarName"-"$jarVersion"-img"