#!/usr/bin/env bash

export  confFile=./conf/hnf-dev-one.yaml
export  jarVersion=1.0.0
export  jarName=event-handler
export  jarFile=$jarName"-"$jarVersion"-jar-with-dependencies.jar"