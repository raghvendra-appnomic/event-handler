#!/usr/bin/env bash
source properties.sh

if [ ! -d logs ] ; then
    mkdir logs
fi

if [ -e $jarFile ]
then
    java -jar $jarFile -c=$confFile
else
   echo "jar file not found..triggering build"
   bash ./build.sh
   java -jar $jarFile -c=$confFile
fi