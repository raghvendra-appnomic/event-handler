package com.appnomic.heal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.constants.Constants;
import com.appnomic.heal.etl.AbstractExtractor;
import com.appnomic.heal.etl.loaders.SysoutLoader;
import com.appnomic.heal.mapping.HostKPIMapping;
import com.appnomic.heal.mapping.InstanceKPIMapping;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public class Adapter {

    final static Logger log = LoggerFactory.getLogger(Adapter.class);

    private final CommandLineParser parser = new DefaultParser();
    private final Options options = new Options();
    private final Option yamlConfig = new Option("c", "cfg", true, "commandline switch for config file path");
    private final Option hostMap = new Option("h", "host", true, "commandline switch for Host KPI Mapping file path");
    private final Option instanceMap = new Option("i", "instance", true, "commandline switch for Instance KPI Mapping file path");

    private final ObjectMapper objectMapperYaml = new ObjectMapper(new YAMLFactory());
    private final ObjectMapper objectMapperJson = new ObjectMapper();


    private static AdapterConfiguration adapterConfiguration;

    private AdapterConfiguration initDefaultConfig() {
        try {
            InputStream defaultConfig = Adapter.class.getClassLoader().getResourceAsStream(Constants.DEFAULT_CONFIG_FILE_NAME);
            if (defaultConfig == null) {
                log.debug("Default config file not found it is supposed to be  {}", System.getProperty("user.dir"));
                throw new FileNotFoundException("Default Configuration File not found.");
            }
            return objectMapperYaml.readValue(defaultConfig, AdapterConfiguration.class);
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException {}", e);
        } catch (JsonParseException e) {
            log.error("JsonParseException occurred {}", e);
        } catch (JsonMappingException e) {
            log.error("JsonMappingException occurred {}", e);
        } catch (IOException e) {
            log.error("IOException occurred {}", e);
        }
        return null;
    }

    private void init() {
        yamlConfig.setRequired(true);
        hostMap.setRequired(true);
        instanceMap.setRequired(true);
        options.addOption(yamlConfig);
        options.addOption(hostMap);
        options.addOption(instanceMap);
        objectMapperYaml.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    private void initCache() {
        if (adapterConfiguration != null) {
            MasterCache.getInstance().setAllRepository(adapterConfiguration.getRepositoriez());
            MasterCache.getInstance().setMetricReportingDir(adapterConfiguration.getCsvDir());
            MasterCache.getInstance().setAccountIdentifier(adapterConfiguration.getAccountIdentifier());
            log.info("CACHE INITIALIZED");
        }
    }

    private void initRepositories() {
        if (adapterConfiguration != null) {
            adapterConfiguration.getRepositoriez().forEach((key, value) -> value.initialize());
        }
    }

    private void loadConfigData() {
        MasterCache.getInstance().getComponentAgentInstancesKPIMap();
        MasterCache.getInstance().getLogForwarderAgentInstancesMap();
    }

    private void initWorkers() {
        if (adapterConfiguration != null) {
            adapterConfiguration.getChainz()
                    .stream()
                    .filter(chain -> chain.getDisableChain() == null || !chain.getDisableChain())
                    .forEach(chain -> {

                        if (chain.getAddSysoutLoader() != null && chain.getAddSysoutLoader()) {
                            chain.getChain().add(new SysoutLoader());
                        }

                        chain.getChain().forEach(worker -> {
                            try {
                                worker.initialize();
                                if (worker.isInitialized()) {
                                    log.info("Worker {} initialized successfully!", worker.getClassName());
                                } else {
                                    log.info("Worker {} NOT INITIALIZED!!", worker.getClassName());
                                }
                            } catch (Exception e) {
                                log.error("Error during initialization of Worker {} : {}", worker.getClassName(), e);
                            }
                        });

                        try {
                            ((AbstractExtractor) chain.getExtractor()).setCurrentChain(chain);
                            chain.getExtractor().initialize();
                            log.info("{} INITIALIZED", chain.getExtractor().getClassName());
                        } catch (Exception e1) {
                            log.error("Error during initialization of Extractor {} : {}", chain.getExtractor().getClassName(), e1);
                        }

                    });
            log.info("All WORKERS INITIALIZED..");
        }
    }

    public void initAdapterConfig(String[] args) {
        init();
        try {
            CommandLine commandLine = parser.parse(options, args);
            String configFileName = commandLine.getOptionValue("cfg");
            String hostKPIMappingFileName = commandLine.getOptionValue("host");
            String instanceKPIMappingFileName = commandLine.getOptionValue("instance");
            File configFile ;
            File hostKPIMappingFile;
            File instanceKPIMappingFile;
            if (configFileName != null && hostKPIMappingFileName != null && instanceKPIMappingFileName != null) {
                configFile = new File(configFileName);
                hostKPIMappingFile = new File(hostKPIMappingFileName);
                instanceKPIMappingFile = new File(instanceKPIMappingFileName);
                if (!configFile.exists() || !hostKPIMappingFile.exists() || !instanceKPIMappingFile.exists()) {
                    log.debug("Current Directory: {} and Config file is supposed to be at {}",
                            System.getProperty("user.dir"), System.getProperty("user.dir") + configFileName);
                    throw new FileNotFoundException("Configuration File " + configFileName + " not found.");
                }
                adapterConfiguration = objectMapperYaml.readValue(configFile, AdapterConfiguration.class);
                loadMappingFiles(hostKPIMappingFile,instanceKPIMappingFile);
            } else {
                adapterConfiguration = initDefaultConfig();
            }
        } catch (MissingOptionException e) {
            log.error("Mandatory command line argument missing: {}", e);
            adapterConfiguration = initDefaultConfig();
        } catch (ParseException e) {
            log.error("Parse Exception {}", e);
            adapterConfiguration = initDefaultConfig();
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException {}", e);
            adapterConfiguration = initDefaultConfig();
        } catch (JsonParseException e) {
            log.error("JsonParseException occurred {}", e);
            adapterConfiguration = initDefaultConfig();
        } catch (JsonMappingException e) {
            log.error("JsonMappingException occurred {}", e);
            adapterConfiguration = initDefaultConfig();
        } catch (IOException e) {
            log.error("IOException occurred {}", e);
            adapterConfiguration = initDefaultConfig();
        }

        if (adapterConfiguration != null) {
            //must be initialised in the given order
            initRepositories();  //1
            initCache();         //2
            initWorkers();       //3
            loadConfigData();    //4
        }
    }

    public static AdapterConfiguration getAdapterConfiguration() {
        return adapterConfiguration;
    }

    public  void loadMappingFiles(File hostKPIMappingFile, File instanceKPIMappingFile) throws IOException {

        List<HostKPIMapping> hostKPIMappingList = objectMapperJson.readValue(hostKPIMappingFile, new TypeReference<List<HostKPIMapping>>() {});

        Map<String, HostKPIMapping> hostKPIMap = new HashMap<>();
        hostKPIMappingList
                .stream()
                .forEach(hostKPIMapping -> hostKPIMap.put(hostKPIMapping.getKpiNameFromClientData(), hostKPIMapping));

        adapterConfiguration.setHostKPIMap(hostKPIMap);

        List<InstanceKPIMapping> instanceKPIMappingList = objectMapperJson.readValue(instanceKPIMappingFile, new TypeReference<List<InstanceKPIMapping>>() {});
        Map<String, InstanceKPIMapping> instanceKPIMap = new HashMap<>();
        instanceKPIMappingList
                .stream()
                .forEach(instanceKPIMapping -> instanceKPIMap.put(instanceKPIMapping.getKpiNameFromClientData(), instanceKPIMapping));

        adapterConfiguration.setInstanceKPIMap(instanceKPIMap);
    }
}
