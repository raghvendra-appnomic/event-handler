package com.appnomic.heal;

import java.util.List;
import java.util.Map;
import com.appnomic.heal.dao.AbstractRepository;
import com.appnomic.heal.etl.AdapterChain;
import com.appnomic.heal.mapping.HostKPIMapping;
import com.appnomic.heal.mapping.InstanceKPIMapping;

public class AdapterConfiguration {
	private List<AdapterChain> chainz;
	private Map<String, AbstractRepository> repositoriez;
	private String csvDir;
	private String accountIdentifier;
	private Map<String, InstanceKPIMapping> instanceKPIMap;
	private Map<String, HostKPIMapping> hostKPIMap;

	public List<AdapterChain> getChainz() {
		return chainz;
	}

	public void setChainz(List<AdapterChain> chainz) {
		this.chainz = chainz;
	}

	public Map<String, AbstractRepository> getRepositoriez() {
		return repositoriez;
	}

	public void setRepositoriez(Map<String, AbstractRepository> repositoriez) {
		this.repositoriez = repositoriez;
	}

	public String getCsvDir() {
		return csvDir;
	}

	public void setCsvDir(String csvDir) {
		this.csvDir = csvDir;
	}

	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}

	public Map<String, InstanceKPIMapping> getInstanceKPIMap() {
		return instanceKPIMap;
	}

	public void setInstanceKPIMap(Map<String, InstanceKPIMapping> instanceKPIMap) {
		this.instanceKPIMap = instanceKPIMap;
	}

	public Map<String, HostKPIMapping> getHostKPIMap() {
		return hostKPIMap;
	}

	public void setHostKPIMap(Map<String, HostKPIMapping> hostKPIMap) {
		this.hostKPIMap = hostKPIMap;
	}
}
