package com.appnomic.heal;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.etl.IExtractor;
import com.appnomic.heal.etl.IFilter;
import com.appnomic.heal.etl.ILoader;
import com.appnomic.heal.etl.ITransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appnomic.heal.constants.Constants;
import com.appnomic.heal.etl.AdapterChain;
import com.appnomic.heal.dao.NameValueRepository;
import com.appnomic.heal.entities.PersistedNameValuePair;
import com.appnomic.heal.etl.utils.DateHelper;

import io.reactivex.rxjava3.core.BackpressureOverflowStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.flowables.ConnectableFlowable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subscribers.TestSubscriber;

public class AdapterETLExecutor {

	private final AdapterChain adapterChain;
	private static final Logger logger = LoggerFactory.getLogger(AdapterETLExecutor.class);
	private final ExecutorService threadPoolExecSvc;

	public AdapterETLExecutor(AdapterChain chain) {
		this.adapterChain = chain;
		if (this.adapterChain.getThreadPoolSize() == null) {
			this.adapterChain.setThreadPoolSize(1); // Default Value
		}
		if (this.adapterChain.getThreadPoolSize() != null) {
			threadPoolExecSvc = Executors.newFixedThreadPool(this.adapterChain.getThreadPoolSize());
		} else {
			threadPoolExecSvc = null;
		}
	}

	public void execute() throws Exception {
		if (this.adapterChain == null) {
			throw new Exception("Chain not initialized!");
		}

		NameValueRepository nameValueRepository = (NameValueRepository) MasterCache.getInstance()
				.getRepositoryByName(Constants.NAME_VALUE_REPOSITORY);

		Date upperThreshold = new Date();
		Date lowerThreshold = null;

		String lastThresholdKey = this.adapterChain.getChainId() + Constants.LAST_UPPER_THRESHOLD;

		String lastUpperThreshold = nameValueRepository.get(lastThresholdKey);

		if (lastUpperThreshold == null || lastUpperThreshold.isEmpty()) {
			// By default, for the first run get the last 5 mins data
			Calendar cal = Calendar.getInstance();
			cal.setTime(upperThreshold);
			cal.add(Calendar.SECOND, -1 * adapterChain.getSleepInterval());
			lowerThreshold = cal.getTime();
		} else {
			lowerThreshold = DateHelper.string2dateConverter(lastUpperThreshold);
		}

		logger.info("Executing chain {} for data from {} to {}", adapterChain.getChainId(),
				DateHelper.date2stringConverter(lowerThreshold), DateHelper.date2stringConverter(upperThreshold));

		// NOT TO BE USED FOR PRODUCTION
		if (adapterChain.getTestStartDate() != null && adapterChain.getTestEndDate() != null) {
//			testStartDate = lowerThreshold;
//			testEndDate = upperThreshold;
			lowerThreshold = adapterChain.getTestStartDate();
			upperThreshold = adapterChain.getTestEndDate();
			logger.info("TEST DATE {} to {} SET. NOT TO BE USED IN PRODUCTION.", lowerThreshold, upperThreshold);
		}

		String strLowerThreshold = DateHelper.date2stringConverter(lowerThreshold);
		String strUpperThreshold = DateHelper.date2stringConverter(upperThreshold);

		lowerThreshold = adapterChain.getAdjustedDate(lowerThreshold);
		Date savedUpperThreshold = (Date) upperThreshold.clone();
		upperThreshold = adapterChain.getAdjustedDate(upperThreshold);

		logger.info("Waking up at {} to execute chain {} for data between {} and {}", strUpperThreshold,
				this.adapterChain.getChainId(), strLowerThreshold, strUpperThreshold);

		long start = System.currentTimeMillis();
		List<AdapterItem> itemz = ((IExtractor) this.adapterChain.getExtractor()).extract(lowerThreshold,
				upperThreshold);

		if (itemz != null) {
			logger.info("Chain {} extracted {} items.", adapterChain.getChainId(), itemz.size());
		} else {
			logger.info("Chain {} extracted NO items.", adapterChain.getChainId());
		}

		long extr = System.currentTimeMillis();
		if (itemz != null && itemz.size() > 0) {

			AdapterItem[] arrItemz = new AdapterItem[itemz.size()];
			itemz.toArray(arrItemz);
			Observable<AdapterItem> itemzObservable = Observable.fromArray(arrItemz);

			// If no thread pool size is set, let the subscribers execute on same thread as
			// the extractor
			itemzObservable.observeOn(
					threadPoolExecSvc != null ? Schedulers.from(threadPoolExecSvc) : Schedulers.computation());

			Flowable<AdapterItem> flowable = itemzObservable.toFlowable(adapterChain.getBackPressureStrategy());
			// If not configured, default back pressure max size = 1024
			flowable.onBackpressureBuffer(
					(adapterChain.getBackPressureMaxSize() != null ? adapterChain.getBackPressureMaxSize() : 1024),
					() -> {
					}, BackpressureOverflowStrategy.DROP_OLDEST);
			ConnectableFlowable<AdapterItem> connectableFlowable = flowable.publish();

			this.adapterChain.getChain().forEach(chain -> {
//				if (chain instanceof ILoader) {
//					connectableFlowable.observeOn(Schedulers.io());
//				}
				connectableFlowable.subscribeWith(new TestSubscriber<AdapterItem>() {
					@Override
					public void onNext(AdapterItem t) {

						if (!t.isDiscardItem()) {
							if (chain instanceof ITransformer) {
								((ITransformer) chain).transform(t);
							} else if (chain instanceof ILoader) {
								((ILoader) chain).load(t);
							} else if (chain instanceof IFilter) {
								Boolean dropItem = ((IFilter) chain).dropItem(t);
								if (dropItem) {
									t.setDiscardItem(true);
								}
							}
						} else {
							logger.info("Item {} will NOT be processed by {} since it has be marked as dropped", t,
									chain);
						}

					}
				});
			});
			connectableFlowable.connect();
		} else {
			logger.info("No new items to process for chain {} for data between {} and {}",
					this.adapterChain.getChainId(), DateHelper.date2stringConverter(lowerThreshold),
					DateHelper.date2stringConverter(upperThreshold));
		}

		PersistedNameValuePair ps = new PersistedNameValuePair(lastThresholdKey,
				DateHelper.date2stringConverter(savedUpperThreshold));
		nameValueRepository.save(ps);
		logger.info("Chain {} Extraction complete in {} ms. Overall Execution complete in {} ms",
				adapterChain.getChainId(), System.currentTimeMillis() - extr, System.currentTimeMillis() - start);
	}
}
