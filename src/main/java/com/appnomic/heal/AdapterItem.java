package com.appnomic.heal;

public class AdapterItem {

	private Object sourceItem;
	private Object destItem;

	@Override
	public String toString() {
		String str = "AdapterItem [discardItem=" + discardItem + ", Class Name=" + getClass() + ", SOURCE: ";
		if (sourceItem != null) {
			str += sourceItem.toString();
		} else {
			str += "NULL";
		}
		str += ", DESTINATION: ";
		if (destItem != null) {
			str += destItem.toString();
		} else {
			str += "NULL";
		}
		str += " ]";
		return str;
	}

	protected boolean discardItem;

	public boolean isDiscardItem() {
		return discardItem;
	}

	public void setDiscardItem(boolean discardItem) {
		this.discardItem = discardItem;
	}

	public Object getSourceItem() {
		return sourceItem;
	}

	public void setSourceItem(Object sourceItem) {
		this.sourceItem = sourceItem;
	}

	public Object getDestItem() {
		return destItem;
	}

	public void setDestItem(Object destItem) {
		this.destItem = destItem;
	}
}
