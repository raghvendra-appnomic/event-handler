package com.appnomic.heal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {
    final static Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        Adapter adapter = new Adapter();
        adapter.initAdapterConfig(args);

        AdapterConfiguration config = adapter.getAdapterConfiguration();

        int threadPoolSize = (int) config.getChainz()
                .stream()
                .filter(chain -> chain.getSleepInterval() != null)
                .filter(chain -> chain.getDisableChain() == null || !chain.getDisableChain())
                .count();

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(threadPoolSize);

        config.getChainz()
                .stream()
                .filter(chain -> chain.getSleepInterval() != null)
                .filter(chain -> chain.getDisableChain() == null || !chain.getDisableChain())
                .forEach(chain -> {

                    AdapterETLExecutor ETLExecutor = new AdapterETLExecutor(chain);
                    Runnable executorThread = () -> {
                        try {
                            ETLExecutor.execute();
                        } catch (Exception e) {
                            log.error("Exception during execution of chain {} : {}", chain.getChainId(), e);
                        }
                    };
                    log.info("Scheduling a new Thread for Extractor of chain {} ever {} seconds", chain.getChainId(), chain.getSleepInterval());

                    scheduledExecutorService.scheduleAtFixedRate(executorThread, 0, chain.getSleepInterval(), TimeUnit.SECONDS);
                });
    }
}
