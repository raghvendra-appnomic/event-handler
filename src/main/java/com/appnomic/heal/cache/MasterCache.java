package com.appnomic.heal.cache;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.appnomic.appsone.eventhandler.pojo.accountconfiguration.AgentConfig;
import com.appnomic.appsone.eventhandler.pojo.response.AccountConfiguration;
import com.appnomic.appsone.eventhandler.pojo.response.ApplicationResponsePojo;
import com.appnomic.appsone.eventhandler.pojo.response.ServiceResponsePojo;
import com.appnomic.heal.constants.Constants;
import com.appnomic.heal.dao.AbstractRepository;
import com.appnomic.heal.mapping.HostKPIMapping;
import com.appnomic.heal.mapping.InstanceKPIMapping;
import com.appnomic.heal.mapping.KPIDetails;
import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.CsvReporter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MasterCache {

    final static Logger log = LoggerFactory.getLogger(MasterCache.class);

    private static MasterCache instance = null;
    private static final Integer MAX_SIZE = Integer.parseInt(Constants.CACHE_MAXIMUM_SIZE_DEFAULT_VALUE);
    private static final Integer CACHE_TIMEOUT = Integer.parseInt(Constants.CACHE_TIMEOUT_IN_MINUTES_DEFAULT_VALUE);

    private final MetricRegistry metrics = new MetricRegistry();
    private ConsoleReporter consoleReporter;
    private CsvReporter csvReporter;

    /**
     * Key : 'allRepository'
     * value : map of all named repositories present in config file
     */
    private LoadingCache<String, Map<String, AbstractRepository>> allRepository = CacheBuilder.newBuilder()
            .maximumSize(MAX_SIZE)
            .expireAfterWrite(CACHE_TIMEOUT, TimeUnit.MINUTES)
            .build(new CacheLoader<String, Map<String, AbstractRepository>>() {
                @Override
                public Map<String, AbstractRepository> load(String s) throws Exception {
                    return allRepository.get("allRepository");
                }
            });

    /**
     * Key : 'metricReportingDir'
     * value : Metric reporting directory where performance events will be logged in csv files
     */
    private LoadingCache<String, String> metricReportingDir = CacheBuilder.newBuilder()
            .maximumSize(MAX_SIZE)
            .expireAfterWrite(CACHE_TIMEOUT, TimeUnit.MINUTES)
            .build(new CacheLoader<String, String>() {
                @Override
                public String load(String s) throws Exception {
                    return metricReportingDir.get("metricReportingDir");
                }
            });

    /**
     * Key : 'accountIdentifier'
     * value : Account identifier from HEAL
     */
    private LoadingCache<String, String> accountIdentifier = CacheBuilder.newBuilder()
            .maximumSize(MAX_SIZE)
            .expireAfterWrite(CACHE_TIMEOUT, TimeUnit.MINUTES)
            .build(new CacheLoader<String, String>() {
                @Override
                public String load(String s) throws Exception {
                    return accountIdentifier.get("accountIdentifier");
                }
            });

    /**
     * Key : {accountIdentifier}
     * value : List of services for a given account in HEAL
     */
    private LoadingCache<String, List<ServiceResponsePojo>> serviceList = CacheBuilder.newBuilder()
            .maximumSize(MAX_SIZE)
            .expireAfterWrite(CACHE_TIMEOUT, TimeUnit.MINUTES)
            .build(new CacheLoader<String, List<ServiceResponsePojo>>() {
                @Override
                public List<ServiceResponsePojo> load(String s) throws Exception {
                    return MasterDataService.getServiceList(s);
                }
            });

    /**
     * Key : {accountIdentifier}
     * value : List of applications for a given account in HEAL
     */
    private LoadingCache<String, List<ApplicationResponsePojo>> applicationList = CacheBuilder.newBuilder()
            .maximumSize(MAX_SIZE)
            .expireAfterWrite(CACHE_TIMEOUT, TimeUnit.MINUTES)
            .build(new CacheLoader<String, List<ApplicationResponsePojo>>() {
                @Override
                public List<ApplicationResponsePojo> load(String s) throws Exception {
                    return MasterDataService.getApplicationList(s);
                }
            });

    /**
     * Key : {"transactionConfiguration"}
     * value : List of transaction configuration for all accounts in HEAL
     */
    private LoadingCache<String, List<AccountConfiguration>> transactionConfiguration = CacheBuilder.newBuilder()
            .maximumSize(MAX_SIZE)
            .expireAfterWrite(CACHE_TIMEOUT, TimeUnit.MINUTES)
            .build(new CacheLoader<String, List<AccountConfiguration>>() {
                @Override
                public List<AccountConfiguration> load(String s) throws Exception {
                    return MasterDataService.getTxnConfigData();
                }
            });

    /**
     * Key : {"instanceKpiMapping"}
     * value : Map of client or external kpi and heal kpi for instances
     */
    private LoadingCache<String, Map<String, InstanceKPIMapping>> instanceKpiMapping = CacheBuilder.newBuilder()
            .maximumSize(MAX_SIZE)
            .expireAfterWrite(CACHE_TIMEOUT, TimeUnit.MINUTES)
            .build(new CacheLoader<String, Map<String, InstanceKPIMapping>>() {
                @Override
                public Map<String, InstanceKPIMapping> load(String s) throws Exception {
                    return MasterDataService.getInstanceKPIMapping();
                }
            });

    /**
     * Key : {"hostKpiMapping"}
     * value : Map of client or external kpi and heal kpi for hosts
     */
    private LoadingCache<String, Map<String, HostKPIMapping>> hostKpiMapping = CacheBuilder.newBuilder()
            .maximumSize(MAX_SIZE)
            .expireAfterWrite(CACHE_TIMEOUT, TimeUnit.MINUTES)
            .build(new CacheLoader<String, Map<String, HostKPIMapping>>() {
                @Override
                public Map<String, HostKPIMapping> load(String s) throws Exception {
                    return MasterDataService.getHostKPIMapping();
                }
            });

    /**
     * Key : {"currentAccount"}
     * value : Map of client or external kpi and heal kpi for hosts
     */
    private LoadingCache<String, Optional<AccountConfiguration>> currentAccount = CacheBuilder.newBuilder()
            .maximumSize(MAX_SIZE)
            .expireAfterWrite(CACHE_TIMEOUT, TimeUnit.MINUTES)
            .build(new CacheLoader<String, Optional<AccountConfiguration>>() {
                @Override
                public Optional<AccountConfiguration> load(String s) throws Exception {
                    return MasterDataService.getCurrentAccountConfig();
                }
            });

    /**
     * Key : {"agentList"}
     * value : Map of client or external kpi and heal kpi for hosts
     */
    private LoadingCache<String, List<AgentConfig>> agentListForCurrentAccount = CacheBuilder.newBuilder()
            .maximumSize(MAX_SIZE)
            .expireAfterWrite(CACHE_TIMEOUT, TimeUnit.MINUTES)
            .build(new CacheLoader<String, List<AgentConfig>>() {
                @Override
                public List<AgentConfig> load(String s) throws Exception {
                    return MasterDataService.getAgentListForCurrentAccount();
                }
            });

    /**
     * Key : {"componentAgentInstancesKPIMap"}
     * value : Mapping agentId of type 'ComponentAgent' with component instances and their non-group KPIs
     */
    private LoadingCache<String, Map<String, Map<String, List<KPIDetails>>>> componentAgentInstancesKPIMap = CacheBuilder.newBuilder()
            .maximumSize(MAX_SIZE)
            .expireAfterWrite(CACHE_TIMEOUT, TimeUnit.MINUTES)
            .build(new CacheLoader<String, Map<String, Map<String, List<KPIDetails>>>>() {
                @Override
                public Map<String, Map<String, List<KPIDetails>>> load(String s) throws Exception {
                    return MasterDataService.getComponentAgentInstancesKPIMap();
                }
            });

    /**
     * Key : {"logForwarderAgentInstancesMap"}
     * value : Mapping agentId of type 'LogForwarder' with component instances identifier
     */
    private LoadingCache<String, Map<String, String>> logForwarderAgentInstancesMap = CacheBuilder.newBuilder()
            .maximumSize(MAX_SIZE)
            .expireAfterWrite(CACHE_TIMEOUT, TimeUnit.MINUTES)
            .build(new CacheLoader<String, Map<String, String>>() {
                @Override
                public Map<String, String> load(String s) throws Exception {
                    return MasterDataService.getLogForwarderAgentInstancesMap();
                }
            });


    /*
     * retrieves all repositories found in cache
     */
    public Map<String, AbstractRepository> getAllRepository() {
        try {
            Map<String, AbstractRepository> allRepositoryList = allRepository.get("allRepository");
            return allRepositoryList;
        } catch (Exception e) {
            log.debug("Error occurred while fetching repositories from cache");
        }
        return null;
    }

    /**
     * Initializes cache with repositories found in configuration file
     */
    public boolean setAllRepository(Map<String, AbstractRepository> repositories) {
        boolean initialized;
        try {
            allRepository.put("allRepository", repositories);
            initialized = true;
        } catch (Exception e) {
            log.debug("Error initializing repositories in cache");
            initialized = false;
        }
        return initialized;
    }

    /**
     * retrieves a given repository from cache
     */
    public AbstractRepository getRepositoryByName(String repoName) {
        try {
            return getAllRepository().get(repoName);
        } catch (Exception e) {
            log.debug("Error occurred while fetching repository from cache");
        }
        return null;
    }

    /*
     * Initializes the reporting directory for logging performance metrics
     */
    public boolean setMetricReportingDir(String directory) {
        boolean initialized = false;
        try {
            if (directory != null) {
                metricReportingDir.put("metricReportingDir", directory);
                consoleReporter = ConsoleReporter.forRegistry(metrics)
                        .convertRatesTo(TimeUnit.SECONDS)
                        .convertDurationsTo(TimeUnit.MILLISECONDS)
                        .build();
                File csvFile = new File(directory);
                csvReporter = CsvReporter.forRegistry(metrics)
                        .convertRatesTo(TimeUnit.SECONDS)
                        .convertDurationsTo(TimeUnit.MILLISECONDS)
                        .build(csvFile);
                csvReporter.start(5, TimeUnit.SECONDS);
                initialized = true;
            }
        } catch (Exception e) {
            log.debug("Error initializing metric reporting directory in cache");
            initialized = false;
        }
        return initialized;
    }

    public Meter getMetricsMeter(String name) {
        return metrics.meter(name);
    }

    /*
     * Initializes cache with Account Identifier found in configuration file
     */
    public boolean setAccountIdentifier(String accId) {
        boolean initialized;
        try {
            accountIdentifier.put(Constants.ACCOUNT_IDENTIFIER, accId);
            initialized = true;
        } catch (Exception e) {
            log.debug("Error initializing accountIdentifier in cache");
            initialized = false;
        }
        return initialized;
    }

    /**
     * retrieves  account identifier from cache
     */
    public String getAccountIdentifier() {
        try {
            return accountIdentifier.get(Constants.ACCOUNT_IDENTIFIER);
        } catch (Exception e) {
            log.debug("Error occurred while fetching accountIdentifier from cache");
        }
        return null;
    }

    /**
     * retrieves list of services for a given account identifier from cache
     *
     * @return
     */
    public List<ServiceResponsePojo> getServiceList(String accountIdentifier, boolean inValidate) {
        try {
            if (inValidate) {
                serviceList.invalidate(accountIdentifier);
            }
            return serviceList.get(accountIdentifier);
        } catch (Exception e) {
            log.debug("Error occurred while fetching service list from cache");
        }
        return null;
    }

    /**
     * retrieves a service using serviceName for a given account identifier from cache
     *
     * @return
     */
    public ServiceResponsePojo getServiceByName(String accountIdentifier, String serviceName) {
        try {
            return serviceList.get(accountIdentifier)
                    .stream()
                    .filter(s -> s.getName().equals(serviceName))
                    .findFirst().get();
        } catch (Exception e) {
            log.debug("Error occurred while fetching service name from cache");
        }
        return null;
    }

    /**
     * retrieves list of applications for a given account identifier from cache
     *
     * @return
     */
    public List<ApplicationResponsePojo> getApplicationList(String accountIdentifier, boolean inValidate) {
        try {
            if (inValidate) {
                applicationList.invalidate(accountIdentifier);
            }
            return applicationList.get(accountIdentifier);
        } catch (Exception e) {
            log.debug("Error occurred while fetching application list from cache");
        }
        return null;
    }

    /**
     * retrieves list of Transaction Configuration for all accounts
     *
     * @return
     */
    public List<AccountConfiguration> getTxnConfigData() {
        try {
            return transactionConfiguration.get("transactionConfiguration");
        } catch (Exception e) {
            log.debug("Error occurred while fetching txn-config-data from cache");
        }
        return null;
    }

    /**
     * retrieves Map of client or external kpi and heal kpi for instances
     *
     * @return
     */
    public Map<String, InstanceKPIMapping> getInstanceKpiMapping() {
        try {
            return instanceKpiMapping.get("instanceKpiMapping");
        } catch (Exception e) {
            log.debug("Error occurred while fetching instance kpi mapping from cache");
        }
        return null;
    }

    /**
     * retrieves Map of client or external kpi and heal kpi for host
     *
     * @return
     */
    public Map<String, HostKPIMapping> getHostKpiMapping() {
        try {
            return hostKpiMapping.get("hostKpiMapping");
        } catch (Exception e) {
            log.debug("Error occurred while fetching host kpi mapping from cache");
        }
        return null;
    }

    /**
     * retrieves the configuration for the current account configured in yaml
     *
     * @return
     */
    public Optional<AccountConfiguration> getCurrentAccountConfig() {
        try {
            return currentAccount.get("currentAccount");
        } catch (Exception e) {
            log.debug("Error occurred while fetching current account configuration from cache");
        }
        return null;
    }

    /**
     * retrieves the list of agents for the current account
     *
     * @return
     */
    public List<AgentConfig> getAgentListForCurrentAccount() {
        try {
            return agentListForCurrentAccount.get("agentList");
        } catch (Exception e) {
            log.debug("Error occurred while fetching agent list from cache");
        }
        return null;
    }

    /**
     * retrieves componentAgentInstancesKPIMap for the current account
     *
     * @return
     */
    public Map<String, Map<String, List<KPIDetails>>> getComponentAgentInstancesKPIMap() {
        try {
            return componentAgentInstancesKPIMap.get("componentAgentInstancesKPIMap");
        } catch (Exception e) {
            log.debug("Error occurred while fetching componentAgentInstancesKPIMap from cache");
        }
        return null;
    }

    /**
     * retrieves logForwarderAgentInstancesMap for the current account
     *
     * @return
     */
    public Map<String, String> getLogForwarderAgentInstancesMap() {
        try {
            return logForwarderAgentInstancesMap.get("logForwarderAgentInstancesMap");
        } catch (Exception e) {
            log.debug("Error occurred while fetching logForwarderAgentInstancesMap from cache");
        }
        return null;
    }



    private MasterCache() {
    }

    public static MasterCache getInstance() {
        if (instance == null) {
            synchronized (MasterCache.class) {
                instance = new MasterCache();
            }
        }
        return instance;
    }
}
