package com.appnomic.heal.cache;

import com.appnomic.appsone.eventhandler.pojo.accountconfiguration.AgentConfig;
import com.appnomic.appsone.eventhandler.pojo.accountconfiguration.CompInstanceClusterConfig;
import com.appnomic.appsone.eventhandler.pojo.response.AccountConfiguration;
import com.appnomic.appsone.eventhandler.pojo.response.ApplicationResponsePojo;
import com.appnomic.appsone.eventhandler.pojo.response.ServiceResponsePojo;
import com.appnomic.appsone.eventhandler.pojo.response.TokenPojo;
import com.appnomic.heal.Adapter;

import com.appnomic.heal.constants.Constants;
import com.appnomic.heal.etl.utils.APIUtil;
import com.appnomic.heal.mapping.HostKPIMapping;
import com.appnomic.heal.mapping.InstanceKPIMapping;
import com.appnomic.heal.mapping.KPIDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class MasterDataService {

    private static Map<String, List<AccountConfiguration>> agentComponentInstanceDetails = new HashMap<>();

    final static Logger log = LoggerFactory.getLogger(MasterDataService.class);

    public static List<ServiceResponsePojo> getServiceList(String accountIdentifier) {
        TokenPojo token = APIUtil.getToken();
        return APIUtil.getServices(accountIdentifier, token);
    }

    public static List<ApplicationResponsePojo> getApplicationList(String accountIdentifier) {
        TokenPojo token = APIUtil.getToken();
        return APIUtil.getApplications(accountIdentifier, token);
    }

    public static List<AccountConfiguration> getTxnConfigData() {
        log.info("Fetching config data from control center...");
        return APIUtil.getTxnConfigData();
    }

    public static List<AccountConfiguration> getAgentConfigData(String accountIdentifier) {
        return APIUtil.getAgentConfigData(accountIdentifier);
    }

    public static Optional<AccountConfiguration> getCurrentAccountConfig() {
        List<AccountConfiguration> txnConfig = MasterCache.getInstance().getTxnConfigData();

        return txnConfig.stream()
                .filter(acc -> acc.getAccountId().equalsIgnoreCase(MasterCache.getInstance().getAccountIdentifier()))
                .findFirst();
    }

    public static List<AgentConfig> getAgentListForCurrentAccount() {
        Optional<AccountConfiguration> currentAccConfig = MasterCache.getInstance().getCurrentAccountConfig();
        List<AgentConfig> agentsListForAccount = null;
        if (currentAccConfig.isPresent()) {
            agentsListForAccount = currentAccConfig.get().getAgentList();
            getAgentComponentInstanceDetails(agentsListForAccount);
        }
        return agentsListForAccount;
    }

    public static Map<String, Map<String, List<KPIDetails>>> getComponentAgentInstancesKPIMap() {

        Map<String, Map<String, List<KPIDetails>>> componentAgentKPIMap = new HashMap<>();

        List<AgentConfig> agentsListForAccount = MasterCache.getInstance().getAgentListForCurrentAccount();

        agentsListForAccount.stream()
                .filter(agent -> agent.getAgentTypeName().equalsIgnoreCase(Constants.AGENT_TYPE_NAME_COMPONENT_AGENT))
                .forEach((agent) -> {
                    List<AccountConfiguration> agentConfig = agentComponentInstanceDetails.get(agent.getAgentId());
                    if (agentConfig.size() > 0) {
                        AccountConfiguration agentDetails = agentConfig.get(0);
                        Map<String, List<KPIDetails>> componentInstancesKPIMap = getComponentInstancesKPIMap(agentDetails);
                        componentAgentKPIMap.put(agent.getAgentId(), componentInstancesKPIMap);
                    }
                });
        return componentAgentKPIMap;
    }

    public static Map<String, String> getLogForwarderAgentInstancesMap() {

        Map<String, String> logForwarderAgentInstancesMap = new HashMap<>();

        List<AgentConfig> agentsListForAccount = MasterCache.getInstance().getAgentListForCurrentAccount();

        agentsListForAccount.stream()
                .filter(agent -> agent.getAgentTypeName().equalsIgnoreCase(Constants.AGENT_TYPE_NAME_LOG_FORWARDER))
                .forEach((agent) -> {
                    List<AccountConfiguration> agentConfig = agentComponentInstanceDetails.get(agent.getAgentId());
                    if (agentConfig.size() > 0) {
                        AccountConfiguration agentDetails = agentConfig.get(0);
                        agentDetails.getCompInstanceDetail().stream()
                                .forEach(instance -> {
                                    logForwarderAgentInstancesMap.put(instance.getIdentifier(), agent.getAgentId());
                                });
                    }
                });
        agentComponentInstanceDetails.clear();
        return logForwarderAgentInstancesMap;
    }

    public static Map<String, List<KPIDetails>> getComponentInstancesKPIMap(AccountConfiguration agentDetails) {

        Map<String, List<KPIDetails>> componentInstancesKPIMap = new HashMap<>();

        agentDetails.getCompInstanceDetail().stream()
                .forEach(comp -> {
                    List<KPIDetails> nonGroupKPIs = getNonGroupKPIs(comp);
                    componentInstancesKPIMap.put(comp.getIdentifier(), nonGroupKPIs);
                });
        return componentInstancesKPIMap;
    }

    public static List<KPIDetails> getNonGroupKPIs(CompInstanceClusterConfig componentInstance) {
        List<KPIDetails> nonGroupKPIs = new ArrayList<>();

        componentInstance.getKpis().stream().filter(kpi -> kpi.getIsGroup() == false)
                .forEach(kpi -> {
                    KPIDetails kpiDetails = new KPIDetails();
                    kpiDetails.setKpiId(kpi.getId());
                    kpiDetails.setKpiName(kpi.getName());
                    kpiDetails.setKpiType(kpi.getKpiType());
                    kpiDetails.setGroup(false);
                    kpiDetails.setCollectionInterval(kpi.getCollectionInterval());
                    nonGroupKPIs.add(kpiDetails);
                });
        return nonGroupKPIs;
    }

    public static void getAgentComponentInstanceDetails(List<AgentConfig> agentsListForAccount) {

        log.info("Fetching list of agents for the account: {} - start: {} " + MasterCache.getInstance().getAccountIdentifier(), new Date());

        log.info("Total agents found: {}  - {}" + agentsListForAccount.size(), new Date());

        log.info("Fetching component instance and kpi details for each agent: start: {}  " + new Date());

        agentsListForAccount.parallelStream()
                .forEach((agent) -> {
                    List<AccountConfiguration> agentConfig = MasterDataService.getAgentConfigData(agent.getAgentId());
                    log.info("Component instance detail found for agentId: {} " + agent.getAgentId());
                    agentComponentInstanceDetails.put(agent.getAgentId(), agentConfig);
                });
        log.info("Fetching instance details completed: {} " + new Date());
    }

    public static Map<String, HostKPIMapping> getHostKPIMapping() {
        return Adapter.getAdapterConfiguration().getHostKPIMap();
    }

    public static Map<String, InstanceKPIMapping> getInstanceKPIMapping() {
        return Adapter.getAdapterConfiguration().getInstanceKPIMap();
    }
}
