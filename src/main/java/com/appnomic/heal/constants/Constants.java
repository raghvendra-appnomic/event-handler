package com.appnomic.heal.constants;

public class Constants {
	public static final String LAST_UPPER_THRESHOLD = "::LastUpperThreshold";
	public static final String NAME_VALUE_REPOSITORY = "NameValueRepository";
	public static final String REPOSITORY = "repository";
	public static final String ACCOUNT_IDENTIFIER = "accountIdentifier";

	public static final String AGENT_TYPE_NAME_COMPONENT_AGENT = "ComponentAgent";
	public static final String AGENT_TYPE_NAME_LOG_FORWARDER = "LogForwarder";

	public static final String SSL_ENABLED = "ssl-enabled";
	public static final String SSL_CERT = "ssl-cert";
	public static final String GRPC_ENDPOINT = "grpc-address";
	public static final String GRPC_PORT = "grpc-port";
	public static final String RETRY_ATTEMPTS = "max-retry-attempts";
	public static final String RETRY_INTERVAL = "retry-interval";

	// Default Configuration File
	public static final String DEFAULT_CONFIG_FILE_NAME = "hnf-all-chains.yaml";

	// Cache Constants
	public static final String CACHE_MAXIMUM_SIZE_DEFAULT_VALUE = "5000";
	public static final String CACHE_TIMEOUT_IN_MINUTES_DEFAULT_VALUE = "1440";


}
