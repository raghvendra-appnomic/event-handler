package com.appnomic.heal.constants;

public class HttpConstants {
	public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
	public static final String CONTENT_TYPE_HEADER_NAME = "Content-Type";
	public static final String CONTENT_TYPE_JSON = "application/json";
	public static final String ACCEPT_HEADER_NAME = "Accept";
	public static final String ACCEPT_JSON = "application/json";
}
