package com.appnomic.heal.dao;

import java.math.BigInteger;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "className")
public abstract class AbstractRepository {

	protected SessionFactory sessionFactory;
	final static Logger log = LoggerFactory.getLogger(AbstractRepository.class);
	protected Properties properties;
	private String className;

	public void initialize() {
		this.sessionFactory = new Configuration().mergeProperties(properties).configure().buildSessionFactory();
	}

	public void save(Object domainObject) {
		Session ssn = this.sessionFactory.openSession();
		Transaction txn = null;
		try {
			txn = ssn.beginTransaction();
			ssn.saveOrUpdate(domainObject);
			txn.commit();
		} catch (HibernateException hex) {
			log.error("Error saving domain Object [" + domainObject.toString() + "]: {}", hex);
			txn.rollback();
		} finally {
			// ssn.flush();
			ssn.clear();
			ssn.close();
		}
	}

	public void delete(Object domainObject) {
		Session ssn = this.sessionFactory.openSession();
		Transaction txn = null;
		try {
			txn = ssn.beginTransaction();
			ssn.delete(domainObject);
			txn.commit();
		} catch (HibernateException hex) {
			log.error("Error deleting domain Object [" + domainObject.toString() + "]: {}", hex);
			txn.rollback();
		} finally {
			ssn.clear();
			ssn.flush();
			ssn.close();
		}
	}

	public void update(Object domainObject) {
		Session ssn = this.sessionFactory.openSession();
		Transaction txn = null;
		try {
			txn = ssn.beginTransaction();
			ssn.update(domainObject);
			txn.commit();
		} catch (HibernateException hex) {
			log.error("Error updating domain Object [" + domainObject.toString() + "]: {}", hex);
			txn.rollback();
		} finally {
			ssn.clear();
			ssn.flush();
			ssn.close();
		}
	}

	public Object get(Object domainObject, BigInteger id) {
		Session ssn = this.sessionFactory.openSession();
		try {
			domainObject = ssn.get(domainObject.getClass(), id);
		} catch (HibernateException hex) {
			domainObject = null;
			log.error("Error getting domain Object with ID [" + id + "]: {}", hex);
		} finally {
			ssn.clear();
			ssn.flush();
			ssn.close();
		}
		return domainObject;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

}
