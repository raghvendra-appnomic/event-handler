package com.appnomic.heal.dao;

import java.util.ArrayList;
import java.util.List;

import com.appnomic.heal.entities.LogscanEndPoint;
import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogAnalyzerKPIMaster extends AbstractRepository {

	final static Logger log = LoggerFactory.getLogger(LogAnalyzerKPIMaster.class);
	private static final String GET_LOGSCAN_ENDPOINTS = "from LogscanEndPoint where id=:id";

	public LogAnalyzerKPIMaster() {
	}

	@Override
	public void initialize() {
		this.sessionFactory = new Configuration()
				.addAnnotatedClass(LogscanEndPoint.class)
				.mergeProperties(properties)
				.configure()
				.buildSessionFactory();
	}

	public LogscanEndPoint fetchLogscanEndpoint(Integer endPtId) {
		List<LogscanEndPoint> endPtz = new ArrayList<>();
		try (Session ssn = this.sessionFactory.openSession()) {
			Query<LogscanEndPoint> query = ssn.createQuery(GET_LOGSCAN_ENDPOINTS);
			query.setParameter("id", endPtId);
			endPtz = query.list();
		} catch (HibernateException hex) {
			log.error("Error fetching latest alerts: {}", hex);
		}
		if (endPtz.size() > 0) {
			return endPtz.get(0);
		}
		return null;
	}
}
