package com.appnomic.heal.dao;

import com.appnomic.heal.entities.PersistedNameValuePair;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

public class NameValueRepository extends AbstractRepository {

	@Override
	public void initialize() {
		this.sessionFactory = new Configuration().addAnnotatedClass(PersistedNameValuePair.class)
				.mergeProperties(properties).configure().buildSessionFactory();
	}

	public void set(String name, String value) {
		PersistedNameValuePair nvp = new PersistedNameValuePair(name, value);
		this.save(nvp);
	}

	public String get(String name) {
		Session ssn = this.sessionFactory.openSession();
		String value = null;
		try {
			PersistedNameValuePair nvp = ssn.get(PersistedNameValuePair.class, name);
			if (nvp != null) {
				value = nvp.getValue();
			}
		} catch (HibernateException hex) {
			value = null;
			log.error("Error getting domain Object with Name [" + name + "]: {}", hex);
		} finally {
			ssn.close();
		}
		return value;
	}

}
