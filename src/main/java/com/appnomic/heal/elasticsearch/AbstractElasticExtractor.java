package com.appnomic.heal.elasticsearch;

import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.constants.Constants;
import com.appnomic.heal.dao.LogAnalyzerKPIMaster;
import com.appnomic.heal.entities.LogscanEndPoint;
import com.appnomic.heal.etl.AbstractExtractor;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig.Builder;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class AbstractElasticExtractor extends AbstractExtractor{

	protected Integer logscanEndPtId;
	protected LogscanEndPoint logscanEndPt = null;
	protected Integer retryAttempts;
	protected Integer maxAttempts = 0;
	protected Integer retryInterval;

	protected RestHighLevelClient elasticClient;
	final static Logger log = LoggerFactory.getLogger(AbstractElasticExtractor.class);

	@Override
	public void initialize() throws Exception {
		super.initialize();
		LogAnalyzerKPIMaster logscanMasterRepository = (LogAnalyzerKPIMaster) MasterCache.getInstance()
				.getRepositoryByName(this.parameters.get(Constants.REPOSITORY));
		if (this.parameters.containsKey(ElasticConstants.ENDPT_KEY)) {
			logscanEndPtId = Integer.valueOf(this.parameters.get(ElasticConstants.ENDPT_KEY));
			logscanEndPt = logscanMasterRepository.fetchLogscanEndpoint(logscanEndPtId);
			if (logscanEndPt == null) {
				log.error("Invalid Logscan Endpoint ID : {}", logscanEndPtId);
			} else {
				Integer connectTimeout = (this.parameters.containsKey(ElasticConstants.CONN_TIMEOUT_KEY)
						? Integer.parseInt(this.parameters.get(ElasticConstants.CONN_TIMEOUT_KEY))
						: 10000);
				Integer socketTimeout = (this.parameters.containsKey(ElasticConstants.SOCKET_TIMEOUT_KEY)
						? Integer.parseInt(this.parameters.get(ElasticConstants.SOCKET_TIMEOUT_KEY))
						: 900000);
				log.debug("Initializing RestHighLevelClient for {}://{}:{}/ with ID {}", logscanEndPt.getProtocol(), logscanEndPt.getHost(), logscanEndPt.getPort(), logscanEndPtId);
				elasticClient = new RestHighLevelClient(RestClient.builder(
						new HttpHost(logscanEndPt.getHost(), logscanEndPt.getPort(), logscanEndPt.getProtocol())).setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
											
							@Override
							public Builder customizeRequestConfig(Builder requestConfigBuilder) {
								return requestConfigBuilder
		                                .setConnectTimeout(connectTimeout)
		                                .setSocketTimeout(socketTimeout);
							}
						}));
				
				this.maxAttempts = (this.parameters.containsKey(ElasticConstants.MAX_ATTEMPTS_KEY)
						? Integer.parseInt(this.parameters.get(ElasticConstants.MAX_ATTEMPTS_KEY))
						: 10);
				this.retryInterval = (this.parameters.containsKey(ElasticConstants.RETRY_INTERVAL_KEY)
						? Integer.parseInt(this.parameters.get(ElasticConstants.RETRY_INTERVAL_KEY))
						: 10);
				this.isInitialized = true;
				log.info("{} Initialized", this.getClass().getName());
			}
		} else {
			this.isInitialized = false;
			log.error("Logscan Endpoint ID Not specified!");
		}
	}

	protected void validateConnection() {
		try {
			pingElasticSearch();
			if (!this.isInitialized) {
				if (this.maxAttempts == -1 || this.retryAttempts < this.maxAttempts) {
					elasticClient.close();
					log.debug("Need to attempt reconnection after {} seconds", this.retryInterval);
					Thread.sleep(1000 * this.retryInterval);

					elasticClient = new RestHighLevelClient(RestClient.builder(
							new HttpHost(logscanEndPt.getHost(), logscanEndPt.getPort(), logscanEndPt.getProtocol())));
					pingElasticSearch();
				} else {
					log.info("Not trying anymore. Retry attempts {} | Retry Limit {}", this.retryAttempts,
							this.maxAttempts);
				}
			}
			if (!this.isInitialized) {
				this.retryAttempts++;
				log.info("Connection initialization failed.");
				validateConnection();
			}
		} catch (Exception ex) {
			log.error("{} during re-initialization : {}", ex.getClass().getName(), ex);
		}
		if (this.isInitialized) {
			log.debug("Connection to elasticsearch endpoint {} working", this.logscanEndPt.getDisplayName());
		}
	}

	protected void pingElasticSearch() {
		try {
			log.debug("Attempting to ping elasticsearch endpoint {}", this.logscanEndPt.getDisplayName());
			this.isInitialized = elasticClient.ping(RequestOptions.DEFAULT);
			log.debug("Received response {} for elasticClient ping", this.isInitialized);
			this.retryAttempts = 0;
			log.debug("ping to elasticsearch endpoint {} successful", this.logscanEndPt.getDisplayName());
		} catch (Exception e) {
			log.error("{} occurred when trying to Ping Elasticsearch {} : {}", e.getClass().getName(),
					this.logscanEndPt.getDisplayName(), e);
			this.isInitialized = false;
		}
	}
}
