package com.appnomic.heal.elasticsearch;

public class ElasticConstants {
	public static final String TIMESTAMP_KEY = "@timestamp";
	public static final String INDEX_KEY = "index-pattern";
	public static final String SCROLLSIZE_KEY = "ls-scroll-size";
	public static final String ENDPT_KEY = "logscanEndPointId";
	public static final String RETRY_INTERVAL_KEY = "elasticsearch-connfail-retryinterval";
	public static final String MAX_ATTEMPTS_KEY = "elasticsearch-connfail-maxattempts";
	public static final String CONN_TIMEOUT_KEY = "elasticsearch-conn-timeout-in-mills";
	public static final String SOCKET_TIMEOUT_KEY = "elasticsearch-socket-timeout-in-mills";

	//default index pattern for HNF

	public static final String HNFW_DEFAULT_INDEX_PATTERN = "heal-hnf-txn-*";
	public static final String HNFM_DEFAULT_INDEX_PATTERN = "heal-hnf-mtr-*";
	public static final String HNFRM_DEFAULT_INDEX_PATTERN = "heal-hnf-rm-*";
	public static final String HNFRW_DEFAULT_INDEX_PATTERN = "heal-hnf-rw-*";
	public static final String HNFRT_DEFAULT_INDEX_PATTERN = "heal-hnf-rt-*";

	public static final String HNF_SETTINGS_INDEX_PATTERN  = "hnfc-settings";

	public static final String TIMESTAMP_PREFIX_HNFTOPR  = "HNFTOPR";
	//default elastic constants
	public static final int ELASTIC_DEFAULT_SCROLL_SIZE = 100;
}
