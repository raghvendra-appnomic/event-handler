package com.appnomic.heal.elasticsearch;

import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.*;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.Objects;

public class ElasticUtil {

    private final static Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));

    final static Logger log = LoggerFactory.getLogger(ElasticUtil.class);

    public static SearchRequest getSearchRequest(Integer scrollSz, String indexPattern) {
        SearchSourceBuilder searchRequestBuilder = new SearchSourceBuilder();

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        searchRequestBuilder.query(boolQueryBuilder);
        searchRequestBuilder.size(scrollSz);

        SearchRequest searchRequest = new SearchRequest(indexPattern);
        searchRequest.source(searchRequestBuilder);
        searchRequest.searchType(SearchType.QUERY_THEN_FETCH);
        searchRequest.scroll(scroll);
        return searchRequest;
    }

    public static SearchResults getSearchHit(RestHighLevelClient elasticClient, SearchRequest searchRequest, String scrollId) {
        SearchResponse searchResponse;
        SearchResults searchResults = new SearchResults();
        if (scrollId == null) {
            try {
                log.debug("Searching Elasticsearch...");
                searchResponse = elasticClient.search(searchRequest, RequestOptions.DEFAULT);
                log.debug("Received response from Elasticsearch: {}", searchResponse);
                String scrlId = searchResponse.getScrollId();
                SearchHit[] searchHits = searchResponse.getHits().getHits();
                searchResults.setSearchHits(searchHits);
                searchResults.setScrollId(scrlId);
            } catch (IOException e) {
                log.error("IO Exception when fetching response from elasticsearch for request {} : {}", searchRequest, e);
            } catch (Exception e) {
                log.error("Exception when fetching response from elasticsearch for request {} : {}", searchRequest, e);
            }
        } else {
            try {
                log.debug("Scrolling Elasticsearch...");
                SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
                scrollRequest.scroll(scroll);
                searchResponse = elasticClient.scroll(scrollRequest, RequestOptions.DEFAULT);
                log.debug("Received scroll response from Elasticsearch: {}", searchResponse);
                scrollId = searchResponse.getScrollId();
                SearchHit[] searchHits = searchResponse.getHits().getHits();
                searchResults.setSearchHits(searchHits);
                searchResults.setScrollId(scrollId);
            } catch (Exception e) {
                log.error("Exception when scrolling elasticsearch results {} : {}", searchRequest, e);
            }
        }
        return searchResults;
    }

    public static boolean stopScrolling(RestHighLevelClient elasticClient, String scrollId) {
        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        ClearScrollResponse clearScrollResponse = null;
        try {
            clearScrollResponse = elasticClient.clearScroll(clearScrollRequest,
                    RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("Exception when clearing scroll: {}", e);
        }
        return Objects.requireNonNull(clearScrollResponse).isSucceeded();
    }

    public static String createElasticRecord(RestHighLevelClient elasticClient,  IndexRequest indexRequest) {
        String id = null;
        try {
            IndexResponse indexResponse = elasticClient.index(indexRequest, RequestOptions.DEFAULT);
            id = indexResponse.getId();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (id != null) ? id : "";
    }

}
