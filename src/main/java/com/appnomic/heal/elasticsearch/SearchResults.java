package com.appnomic.heal.elasticsearch;

import org.elasticsearch.search.SearchHit;

public class SearchResults {
    SearchHit[] searchHits;
    String scrollId;

    public SearchHit[] getSearchHits() {
        return searchHits;
    }

    public void setSearchHits(SearchHit[] searchHits) {
        this.searchHits = searchHits;
    }

    public String getScrollId() {
        return scrollId;
    }

    public void setScrollId(String scrollId) {
        this.scrollId = scrollId;
    }
}
