package com.appnomic.heal.elasticsearch.pojo;

public class LogscanLAKpiRequestItem {
	@Override
	public String toString() {
		return "LogscanLAKpiRequestItem [kpiName=" + kpiName + ", kpiPattern=" + kpiPattern + ", indexPattern="
				+ indexPattern + ", kpiUid=" + kpiUid + "]";
	}

	private String kpiName;
	private String kpiPattern;
	private String serviceName;
	private String indexPattern;
	private String logfileType;
	private Boolean groupKpi;
	private String kpiGroup;
	private Integer kpiUid;
	private String logscanFieldName;

	public String getKpiName() {
		return kpiName;
	}

	public void setKpiName(String kpiName) {
		this.kpiName = kpiName;
	}

	public String getKpiPattern() {
		return kpiPattern;
	}

	public void setKpiPattern(String kpiPattern) {
		this.kpiPattern = kpiPattern;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getIndexPattern() {
		return indexPattern;
	}

	public void setIndexPattern(String indexPattern) {
		this.indexPattern = indexPattern;
	}

	public String getLogfileType() {
		return logfileType;
	}

	public void setLogfileType(String logfileType) {
		this.logfileType = logfileType;
	}

	public Boolean getGroupKpi() {
		return groupKpi;
	}

	public void setGroupKpi(Boolean groupKpi) {
		this.groupKpi = groupKpi;
	}

	public String getKpiGroup() {
		return kpiGroup;
	}

	public void setKpiGroup(String kpiGroup) {
		this.kpiGroup = kpiGroup;
	}

	public Integer getKpiUid() {
		return kpiUid;
	}

	public void setKpiUid(Integer kpiUid) {
		this.kpiUid = kpiUid;
	}

	public String getLogscanFieldName() {
		return logscanFieldName;
	}

	public void setLogscanFieldName(String logscanFieldName) {
		this.logscanFieldName = logscanFieldName;
	}

}
