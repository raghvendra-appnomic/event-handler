package com.appnomic.heal.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dataadapter.a1_logscan_endpt")
public class LogscanEndPoint {
	@Id
	@Column(name = "id")
	private Integer id;
	@Column(name = "port")
	private Integer port;
	@Column(name = "host")
	private String host;
	@Column(name = "protocol")
	private String protocol;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	@Override
	public String toString() {
		return "LogscanEndPoint [id=" + id + ", port=" + port + ", host=" + host + ", protocol=" + protocol + "]";
	}

	public String getDisplayName() {
		return this.protocol + "://" + this.host + ":" + this.port;
	}
}
