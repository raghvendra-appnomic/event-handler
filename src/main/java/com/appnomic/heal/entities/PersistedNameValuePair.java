package com.appnomic.heal.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dataadapter.a1_adapter_state")
public class PersistedNameValuePair {

	@Id
	@Column(name = "name")
	private String name;
	@Column(name = "value")
	private String value;

	public PersistedNameValuePair() {
		this.name = "";
		this.value = "";
	}

	public PersistedNameValuePair(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
