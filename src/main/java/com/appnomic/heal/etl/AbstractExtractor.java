package com.appnomic.heal.etl;

public abstract class AbstractExtractor extends AbstractChainableWorker implements IExtractor {

	protected AdapterChain currentChain;

	public AdapterChain getCurrentChain() {
		return currentChain;
	}

	public void setCurrentChain(AdapterChain currentChain) {
		this.currentChain = currentChain;
	}
}
