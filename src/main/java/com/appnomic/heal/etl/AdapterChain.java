package com.appnomic.heal.etl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.reactivex.rxjava3.core.BackpressureStrategy;

public class AdapterChain {
	private String chainId;
	private AbstractChainableWorker extractor;
	private List<AbstractChainableWorker> chainOfWorkerz;
	private Integer sleepInterval;
	private Integer threadPoolSize;
	private Date testStartDate;
	private Date testEndDate;
	private Boolean addSysoutLoader;
	private Boolean disableChain = false;
	private BackpressureStrategy backPressureStrategy;
	private Integer backPressureMaxSize;
	private Integer deltaInMins;

//	@JsonIgnoreProperties(ignoreUnknown = true)
//	private AdapterConfiguration configuration;

	public Date getAdjustedDate(Date originalDate) {
		if (deltaInMins == null) {
			return originalDate;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(originalDate);
		cal.add(Calendar.MINUTE, -1 * deltaInMins);
		return cal.getTime();
	}

	public List<AbstractChainableWorker> getChainOfWorkerz() {
		return chainOfWorkerz;
	}

	public void setChainOfWorkerz(List<AbstractChainableWorker> chainOfWorkerz) {
		this.chainOfWorkerz = chainOfWorkerz;
	}

	// Default is Buffer
	public BackpressureStrategy getBackPressureStrategy() {
		return (backPressureStrategy != null ? backPressureStrategy : BackpressureStrategy.BUFFER);
	}

	public void setBackPressureStrategy(BackpressureStrategy backPressureStrategy) {
		this.backPressureStrategy = backPressureStrategy;
	}

	public Integer getThreadPoolSize() {
		return threadPoolSize;
	}

	public void setThreadPoolSize(Integer threadPoolSize) {
		this.threadPoolSize = threadPoolSize;
	}

	public void setExtractor(AbstractChainableWorker extractor) {
		this.extractor = extractor;
	}

	public String getChainId() {
		return chainId;
	}

	public void setChainId(String chainId) {
		this.chainId = chainId;
	}

	public Integer getSleepInterval() {
		return sleepInterval;
	}

	public void setSleepInterval(Integer sleepInterval) {
		this.sleepInterval = sleepInterval;
	}

//	public AdapterConfiguration getConfiguration() {
//		return configuration;
//	}
//
//	public void setConfiguration(AdapterConfiguration configuration) {
//		this.configuration = configuration;
//	}

	public AbstractChainableWorker getExtractor() {
		return extractor;
	}

	public Date getTestStartDate() {
		return testStartDate;
	}

	public void setTestStartDate(Date testStartDate) {
		this.testStartDate = testStartDate;
	}

	public Date getTestEndDate() {
		return testEndDate;
	}

	public void setTestEndDate(Date testEndDate) {
		this.testEndDate = testEndDate;
	}

	public List<AbstractChainableWorker> getChain() {
		return chainOfWorkerz;
	}

	public void setChain(List<AbstractChainableWorker> chain) {
		this.chainOfWorkerz = chain;
	}

	public Boolean getAddSysoutLoader() {
		return addSysoutLoader;
	}

	public void setAddSysoutLoader(Boolean addSysoutLoader) {
		this.addSysoutLoader = addSysoutLoader;
	}

	public Boolean getDisableChain() {
		return disableChain;
	}

	public void setDisableChain(Boolean disableChain) {
		this.disableChain = disableChain;
	}

	public Integer getBackPressureMaxSize() {
		return backPressureMaxSize;
	}

	public void setBackPressureMaxSize(Integer backPressureMaxSize) {
		this.backPressureMaxSize = backPressureMaxSize;
	}

	public Integer getDeltaInMins() {
		return deltaInMins;
	}

	public void setDeltaInMins(Integer deltaInMins) {
		this.deltaInMins = deltaInMins;
	}

}
