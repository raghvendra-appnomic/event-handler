package com.appnomic.heal.etl;

import java.util.Date;
import java.util.List;

import com.appnomic.heal.AdapterItem;

public interface IExtractor {
	List<AdapterItem> extract(Date from, Date to);
}
