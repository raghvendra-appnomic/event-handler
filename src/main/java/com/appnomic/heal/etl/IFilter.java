package com.appnomic.heal.etl;

import com.appnomic.heal.AdapterItem;

public interface IFilter {
	Boolean dropItem(AdapterItem item);
}
