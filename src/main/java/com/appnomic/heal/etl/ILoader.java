package com.appnomic.heal.etl;

import com.appnomic.heal.AdapterItem;

public interface ILoader {
	void load(AdapterItem item);
}
