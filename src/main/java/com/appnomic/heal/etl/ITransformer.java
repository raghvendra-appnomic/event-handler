package com.appnomic.heal.etl;

import com.appnomic.heal.AdapterItem;

public interface ITransformer {
	void transform(AdapterItem item);
}
