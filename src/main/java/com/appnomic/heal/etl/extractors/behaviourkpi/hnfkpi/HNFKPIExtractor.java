package com.appnomic.heal.etl.extractors.behaviourkpi.hnfkpi;

import com.appnomic.heal.AdapterItem;
import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.elasticsearch.AbstractElasticExtractor;
import com.appnomic.heal.elasticsearch.ElasticConstants;
import com.appnomic.heal.elasticsearch.ElasticUtil;
import com.appnomic.heal.elasticsearch.SearchResults;
import com.appnomic.heal.etl.extractors.behaviourkpi.hnfkpi.pojo.HNFKPI;
import com.appnomic.heal.etl.extractors.behaviourkpi.hnfkpir.pojo.HNFKPIR;
import com.appnomic.heal.etl.utils.DateHelper;
import com.codahale.metrics.Meter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.*;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class HNFKPIExtractor extends AbstractElasticExtractor {

    final static Logger log = LoggerFactory.getLogger(HNFKPIExtractor.class);
    private final Meter hnfmExtraction = MasterCache.getInstance().getMetricsMeter("hnfm-extraction");
    private SearchRequest searchRequest;
    Integer scrollSz;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void initialize() throws Exception {
        super.initialize();
        if (this.isInitialized) {

            String indexPattern = this.parameters.getOrDefault(ElasticConstants.INDEX_KEY, ElasticConstants.HNFM_DEFAULT_INDEX_PATTERN);
            scrollSz = this.parameters.containsKey(ElasticConstants.SCROLLSIZE_KEY)
                    ? Integer.parseInt(this.parameters.get(ElasticConstants.SCROLLSIZE_KEY))
                    : ElasticConstants.ELASTIC_DEFAULT_SCROLL_SIZE;

            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

            searchRequest = ElasticUtil.getSearchRequest(scrollSz, indexPattern);

            pingElasticSearch();

            this.isInitialized = true;
            log.info("{} initialized", this.className);
        } else {
            log.error("Base class not initialized!");
        }
    }

    @Override
    public List<AdapterItem> extract(Date from, Date to) {
        validateConnection();
        if (!this.isInitialized) {
            log.info("Initialization Error! Cannot execute extraction");
            return null;
        }

        log.info("Extraction from {} to {} by chain {} in Thread {}", DateHelper.date2stringConverter(from),
                DateHelper.date2stringConverter(to), this.currentChain.getChainId(), Thread.currentThread().getId());
        List<AdapterItem> results = new ArrayList<>();
        BoolQueryBuilder boolQueryBuilder = (BoolQueryBuilder) searchRequest.source().query();
        boolQueryBuilder.must().clear();
        boolQueryBuilder.must(QueryBuilders.rangeQuery(ElasticConstants.TIMESTAMP_KEY).gte(from).lte(to));

        log.debug("Request to Elasticsearch : {}", searchRequest);
        SearchResults searchResults = ElasticUtil.getSearchHit(elasticClient, searchRequest, null);

        do {
            for (SearchHit hit : searchResults.getSearchHits()) {
                AdapterItem item = new AdapterItem();
                String _source = hit.getSourceAsString();
                try {
                    HNFKPI HNFKPIEntry = objectMapper.readValue(_source, new TypeReference<HNFKPI>() {});
                    log.debug("HNFKPIEntry: {}", HNFKPIEntry);
                    performDiscovery(HNFKPIEntry);
                    item.setSourceItem(HNFKPIEntry);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                results.add(item);
            }
            if(searchResults.getScrollId()!=null) {
                searchResults = ElasticUtil.getSearchHit(elasticClient, searchRequest, searchResults.getScrollId());
            }
        }
        while (searchResults.getSearchHits() != null && searchResults.getSearchHits().length > 0);

        if(searchResults.getScrollId()!=null) {
            boolean succeeded = ElasticUtil.stopScrolling(elasticClient, searchResults.getScrollId());
            log.info("Scrolling cleared: {} Extracted {} potential transactions", succeeded, results.size());
        }

        hnfmExtraction.mark();
        return results;
    }

    /*
     * Discover if there is any potential new HNFRM record
     * */
    private void performDiscovery(HNFKPI HNFMEntry) {
        log.debug("HNFMEntry: {}", HNFMEntry);
        if (HNFMEntry.getHNFKPI() != null && HNFMEntry.getHNFKPI().getKpiName() != null && HNFMEntry.getHNFKPI().getKpiType() != null
                && HNFMEntry.getHNFKPI().getCompName() != null) {
            boolean metricExists = metricExists(HNFMEntry);
            if (!metricExists) {
                boolean metricCreated = createHNFRMEvent(HNFMEntry);
                if (!metricCreated) {
                    log.info("HNFRM  {} could not be created for:  ", HNFMEntry);
                }
            }
        } else {
            log.info("HNFRM could not be verified for: {} ", HNFMEntry);
        }
    }

    /*
     * Check if there is HNFRM record corresponding to this HNFM record
     * */
    private boolean metricExists(HNFKPI record) {
        log.info("Checking if HNFRM record exists for {}", record);

        String indexPattern = ElasticConstants.HNFRM_DEFAULT_INDEX_PATTERN;
        SearchRequest searchRequest = ElasticUtil.getSearchRequest(scrollSz, indexPattern);

        BoolQueryBuilder boolQueryBuilder = (BoolQueryBuilder) searchRequest.source().query();
        boolQueryBuilder.must().clear();

        boolQueryBuilder.must(QueryBuilders.termQuery("HNFKPI.KpiName.keyword", record.getHNFKPI().getKpiName()));
        boolQueryBuilder.must(QueryBuilders.termQuery("HNFKPI.KpiType.keyword", record.getHNFKPI().getKpiType()));
        boolQueryBuilder.must(QueryBuilders.termQuery("HNFKPI.CompName.keyword", record.getHNFKPI().getCompName()));

        log.debug("Request to Elasticsearch : {}", searchRequest);
        SearchResponse searchResponse;
        try {
            searchResponse = elasticClient.search(searchRequest, RequestOptions.DEFAULT);
            SearchHit[] searchHits = searchResponse.getHits().getHits();
            return (searchHits != null && searchHits.length > 0);
        } catch (Exception e) {
            log.info("Error fetching HNFRM records: {}", e.getMessage());
        }
        return false;
    }

    /*
     * Generate an HNFRM record corresponding to this HNFW record
     * */
    private boolean createHNFRMEvent(HNFKPI record) {

        Map<String, Object> HNFRMJson = HNFKPIR.createJson(record);

        IndexRequest indexRequest = new IndexRequest(HNFKPIR.createIndex(), "doc", HNFKPIR.createId()).source(HNFRMJson);

        String id  = ElasticUtil.createElasticRecord(elasticClient, indexRequest);

        return id != null;
    }

}
