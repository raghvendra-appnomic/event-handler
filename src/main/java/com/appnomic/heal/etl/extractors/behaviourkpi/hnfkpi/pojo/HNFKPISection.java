package com.appnomic.heal.etl.extractors.behaviourkpi.hnfkpi.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HNFKPISection {

    @JsonProperty(value = "@timestamp")
    String timestamp;
    String KpiName;
    String KpiType;
    String TimestampFrom;
    String TimestampUpto;
    int IntervalSeconds;
    float KpiValue;
    String CompName;
    String CompInstance;
    String CompHost;
    String CompResource;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getKpiName() {
        return KpiName;
    }

    public void setKpiName(String kpiName) {
        KpiName = kpiName;
    }

    public String getKpiType() {
        return KpiType;
    }

    public void setKpiType(String kpiType) {
        KpiType = kpiType;
    }

    public String getTimestampFrom() {
        return TimestampFrom;
    }

    public void setTimestampFrom(String timestampFrom) {
        TimestampFrom = timestampFrom;
    }

    public String getTimestampUpto() {
        return TimestampUpto;
    }

    public void setTimestampUpto(String timestampUpto) {
        TimestampUpto = timestampUpto;
    }

    public int getIntervalSeconds() {
        return IntervalSeconds;
    }

    public void setIntervalSeconds(int intervalSeconds) {
        IntervalSeconds = intervalSeconds;
    }

    public float getKpiValue() {
        return KpiValue;
    }

    public void setKpiValue(float kpiValue) {
        KpiValue = kpiValue;
    }

    public String getCompName() {
        return CompName;
    }

    public void setCompName(String compName) {
        CompName = compName;
    }

    public String getCompInstance() {
        return CompInstance;
    }

    public void setCompInstance(String compInstance) {
        CompInstance = compInstance;
    }

    public String getCompHost() {
        return CompHost;
    }

    public void setCompHost(String compHost) {
        CompHost = compHost;
    }

    public String getCompResource() {
        return CompResource;
    }

    public void setCompResource(String compResource) {
        CompResource = compResource;
    }
}
