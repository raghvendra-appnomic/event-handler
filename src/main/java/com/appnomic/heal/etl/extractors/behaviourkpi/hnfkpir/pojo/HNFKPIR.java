package com.appnomic.heal.etl.extractors.behaviourkpi.hnfkpir.pojo;

import com.appnomic.heal.elasticsearch.ElasticConstants;
import com.appnomic.heal.etl.extractors.behaviourkpi.hnfkpi.pojo.HNFKPI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static java.util.UUID.randomUUID;

public class HNFKPIR  {

    HNFKPIRSection HNFKPIR;

    public HNFKPIRSection getHNFKPIR() {
        return HNFKPIR;
    }

    public void setHNFKPIR(HNFKPIRSection HNFKPIR) {
        this.HNFKPIR = HNFKPIR;
    }

    public static Map<String, Object> createJson(HNFKPI FM) {

        Map<String, Object> HNFRMJson = new HashMap<>();

        HNFRMJson.put("HNFKPIR."+ElasticConstants.TIMESTAMP_KEY, new Date());

        HNFRMJson.put("HNFKPIR.Name", FM.getHNFKPI().getKpiName());
        HNFRMJson.put("HNFKPIR.Type", FM.getHNFKPI().getKpiType());

        HNFRMJson.put("HNFKPIR.CompName", FM.getHNFKPI().getCompName());
        HNFRMJson.put("HNFKPIR.CompInstance", FM.getHNFKPI().getCompInstance());
        HNFRMJson.put("HNFKPIR.CompHost", FM.getHNFKPI().getCompHost());

        return HNFRMJson;
    }

    public static String createIndex() {
        String prefix = ElasticConstants.HNFRM_DEFAULT_INDEX_PATTERN.substring(0, ElasticConstants.HNFRM_DEFAULT_INDEX_PATTERN.length() - 2);
        return prefix + "-" + new Date().getTime();
    }

    public static String createId() {
        return randomUUID().toString();
    }
}
