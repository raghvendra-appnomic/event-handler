package com.appnomic.heal.etl.extractors.behaviourkpi.hnfkpir.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HNFKPIRSection {
    @JsonProperty(value = "@timestamp")
    String timestamp;
    String Name;
    String Type;
    String CompName;
    String CompInstance;
    String CompHost;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getCompName() {
        return CompName;
    }

    public void setCompName(String compName) {
        CompName = compName;
    }

    public String getCompInstance() {
        return CompInstance;
    }

    public void setCompInstance(String compInstance) {
        CompInstance = compInstance;
    }

    public String getCompHost() {
        return CompHost;
    }

    public void setCompHost(String compHost) {
        CompHost = compHost;
    }
}
