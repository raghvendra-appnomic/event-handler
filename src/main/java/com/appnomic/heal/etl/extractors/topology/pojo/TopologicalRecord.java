package com.appnomic.heal.etl.extractors.topology.pojo;

public class TopologicalRecord {
    Topology HNFTOPR;

    public Topology getHNFTOPR() {
        return HNFTOPR;
    }

    public void setHNFTOPR(Topology HNFTOPR) {
        this.HNFTOPR = HNFTOPR;
    }
}
