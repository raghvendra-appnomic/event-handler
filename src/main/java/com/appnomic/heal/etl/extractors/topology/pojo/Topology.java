package com.appnomic.heal.etl.extractors.topology.pojo;

import com.appnomic.heal.elasticsearch.ElasticConstants;
import com.appnomic.heal.etl.extractors.workloadkpi.hnftxn.pojo.HNFTXNSection;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.UUID.randomUUID;

public class Topology implements Serializable {
    @JsonProperty(value="@timestamp")
    String timestamp;
    String CompName;
    String CompInstance;
    String CompHost;
    String CompType;

    @JsonProperty(value="CompIsService")
    boolean CompIsService;
    List<String> CompTags;

    public String getCompName() {
        return CompName;
    }

    public void setCompName(String compName) {
        CompName = compName;
    }

    public String getCompInstance() {
        return CompInstance;
    }

    public void setCompInstance(String compInstance) {
        CompInstance = compInstance;
    }

    public String getCompHost() {
        return CompHost;
    }

    public void setCompHost(String compHost) {
        CompHost = compHost;
    }

    public String getCompType() {
        return CompType;
    }

    public void setCompType(String compType) {
        CompType = compType;
    }

    public boolean isCompIsService() {
        return CompIsService;
    }

    public void setCompIsService(boolean compIsService) {
        CompIsService = compIsService;
    }

    public List<String> getCompTags() {
        return CompTags;
    }

    public void setCompTags(List<String> compTags) {
        CompTags = compTags;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public static Map<String, Object> createJson(HNFTXNSection HNFTxn) {

        Map<String, Object> HNFTOPRJson = new HashMap<>();
        HNFTOPRJson.put(ElasticConstants.TIMESTAMP_KEY, new Date());
        HNFTOPRJson.put("CompName", HNFTxn.getServiceName());
        HNFTOPRJson.put("CompInstance", HNFTxn.getServiceInstance());
        HNFTOPRJson.put("CompHost", HNFTxn.getServiceHost());
        HNFTOPRJson.put("CompType", HNFTxn.getServiceType());
        HNFTOPRJson.put("CompIsService", true);
        HNFTOPRJson.put("CompTags",  HNFTxn.getServiceTags());

        Map<String, Object> HNFTOPR = new HashMap<>();

        HNFTOPR.put("HNFTOPR", HNFTOPRJson);

        return HNFTOPRJson;
    }

    public static String createIndex() {
        String prefix = ElasticConstants.HNFRT_DEFAULT_INDEX_PATTERN.substring(0, ElasticConstants.HNFRT_DEFAULT_INDEX_PATTERN.length() - 2);
        return prefix + "-" + new Date().getTime();
    }

    public static String createId() {
        return randomUUID().toString();
    }
}
