package com.appnomic.heal.etl.extractors.workloadkpi.hnftxn;

import java.util.*;
import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.elasticsearch.AbstractElasticExtractor;
import com.appnomic.heal.elasticsearch.ElasticConstants;
import com.appnomic.heal.elasticsearch.ElasticUtil;
import com.appnomic.heal.elasticsearch.SearchResults;
import com.appnomic.heal.etl.extractors.topology.pojo.Topology;
import com.appnomic.heal.etl.extractors.workloadkpi.hnftxn.pojo.HNFTXN;
import com.appnomic.heal.etl.extractors.workloadkpi.hnftxn.pojo.HNFTXNSection;
import com.appnomic.heal.etl.extractors.workloadkpi.hnftxnr.pojo.HNFTXNR;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appnomic.heal.AdapterItem;
import com.appnomic.heal.etl.utils.DateHelper;
import com.codahale.metrics.Meter;

public class HNFTXNExtractor extends AbstractElasticExtractor {

    final static Logger log = LoggerFactory.getLogger(HNFTXNExtractor.class);
    private final Meter hnfwExtraction = MasterCache.getInstance().getMetricsMeter("hnfw-extraction");
    private SearchRequest searchRequest;
    private Integer scrollSz;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void initialize() throws Exception {
        super.initialize();
        if (this.isInitialized) {

            String indexPattern = this.parameters.getOrDefault(ElasticConstants.INDEX_KEY, ElasticConstants.HNFW_DEFAULT_INDEX_PATTERN);
            scrollSz = this.parameters.containsKey(ElasticConstants.SCROLLSIZE_KEY)
                    ? Integer.parseInt(this.parameters.get(ElasticConstants.SCROLLSIZE_KEY))
                    : ElasticConstants.ELASTIC_DEFAULT_SCROLL_SIZE;
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

            searchRequest = ElasticUtil.getSearchRequest(scrollSz, indexPattern);

            pingElasticSearch();

            this.isInitialized = true;
            log.info("{} initialized", this.className);
        } else {
            log.error("Base class not initialized!");
        }
    }

    @Override
    public List<AdapterItem> extract(Date from, Date to) {
        validateConnection();
        if (!this.isInitialized) {
            log.info("Initialization Error! Cannot execute extraction");
            return null;
        }

        log.info("Extraction from {} to {} by chain {} in Thread {}", DateHelper.date2stringConverter(from),
                DateHelper.date2stringConverter(to), this.currentChain.getChainId(), Thread.currentThread().getId());
        List<AdapterItem> results = new ArrayList<>();
        BoolQueryBuilder boolQueryBuilder = (BoolQueryBuilder) searchRequest.source().query();
        boolQueryBuilder.must().clear();
        boolQueryBuilder.must(QueryBuilders.rangeQuery(ElasticConstants.TIMESTAMP_KEY).gte(from).lte(to));

        log.debug("Request to Elasticsearch : {}", searchRequest);

        SearchResults searchResults = ElasticUtil.getSearchHit(elasticClient, searchRequest, null);

        do {
            for (SearchHit hit : searchResults.getSearchHits()) {
                AdapterItem item = new AdapterItem();
                String _source = hit.getSourceAsString();
                try {
                    HNFTXN HNFTXNEntry = objectMapper.readValue(_source, new TypeReference<HNFTXN>() {});
                    performDiscovery(HNFTXNEntry.getHNFTXN());
                    item.setSourceItem(HNFTXNEntry);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                results.add(item);
            }
            if(searchResults.getScrollId()!=null) {
                searchResults = ElasticUtil.getSearchHit(elasticClient, searchRequest, searchResults.getScrollId());
            }
        }
        while (searchResults.getSearchHits() != null && searchResults.getSearchHits().length > 0);

        if(searchResults.getScrollId()!=null) {
            boolean succeeded = ElasticUtil.stopScrolling(elasticClient, searchResults.getScrollId());
            log.info("Scrolling cleared: {} Extracted {} potential transactions", succeeded, results.size());
        }

        hnfwExtraction.mark();
        return results;
    }

    /*
     * Generate an HNRT record corresponding to this HNFW record
     * */
    private boolean createHNFRTEvent(HNFTXNSection record) {

        Map<String, Object> HNFRTJson = Topology.createJson(record);

        IndexRequest indexRequest = new IndexRequest(Topology.createIndex(), "doc", Topology.createId()).source(HNFRTJson);

        String id  = ElasticUtil.createElasticRecord(elasticClient, indexRequest);

        return id != null;
    }

    /*
     * Generate an HNFRW record corresponding to this HNFW record
     * */
    private boolean createHNFRWEvent(HNFTXNSection record) {


        Map<String, Object> HNFRWJson = HNFTXNR.createJson(record);

        IndexRequest indexRequest = new IndexRequest(HNFTXNR.createIndex(), "doc", HNFTXNR.createId()).source(HNFRWJson);

        String id  = ElasticUtil.createElasticRecord(elasticClient, indexRequest);

        return id != null;
    }

    /*
     * Check if there is HNRT record corresponding to this HNFW record
     * */
    private boolean topologyExists(HNFTXNSection record) {
        log.info("Checking if HNFRT record exists for {}", record.getServiceName());

        String indexPattern = ElasticConstants.HNFRT_DEFAULT_INDEX_PATTERN;
        SearchRequest searchRequest = ElasticUtil.getSearchRequest(scrollSz, indexPattern);

        BoolQueryBuilder boolQueryBuilder = (BoolQueryBuilder) searchRequest.source().query();
        boolQueryBuilder.must().clear();

        boolQueryBuilder.must(QueryBuilders.termQuery("Name.keyword", record.getServiceName()));
        boolQueryBuilder.must(QueryBuilders.termQuery("Instance.keyword", record.getServiceInstance()));
        boolQueryBuilder.must(QueryBuilders.termQuery("Host.keyword", record.getServiceHost()));
        boolQueryBuilder.must(QueryBuilders.termQuery("Type.keyword", record.getServiceType()));

        log.debug("Request to Elasticsearch : {}", searchRequest);
        SearchResponse searchResponse;
        try {
            searchResponse = elasticClient.search(searchRequest, RequestOptions.DEFAULT);
            SearchHit[] searchHits = searchResponse.getHits().getHits();
            return (searchHits != null && searchHits.length > 0);
        } catch (Exception e) {
            log.info("Error fetching HNFRT records: {}", e.getMessage());
        }
        return false;
    }

    /*
     * Check if there is HNFRW record corresponding to this HNFW record
     * */
    private boolean workLoadExists(HNFTXNSection record) {

        log.info("Checking if HNFRW record exists for {}", record.getName());

        String indexPattern = ElasticConstants.HNFRW_DEFAULT_INDEX_PATTERN;
        SearchRequest searchRequest = ElasticUtil.getSearchRequest(scrollSz, indexPattern);

        BoolQueryBuilder boolQueryBuilder = (BoolQueryBuilder) searchRequest.source().query();
        boolQueryBuilder.must().clear();
        boolQueryBuilder.must(QueryBuilders.termQuery("Name.keyword", record.getName()));
        boolQueryBuilder.must(QueryBuilders.termQuery("ServiceName.keyword", record.getServiceName()));
        boolQueryBuilder.must(QueryBuilders.termQuery("Direction.keyword", record.getDirection()));

        log.debug("Request to Elasticsearch : {}", searchRequest);
        SearchResponse searchResponse;
        try {
            searchResponse = elasticClient.search(searchRequest, RequestOptions.DEFAULT);
            SearchHit[] searchHits = searchResponse.getHits().getHits();
            return (searchHits != null && searchHits.length > 0);
        } catch (Exception e) {
            log.info("Error fetching HNFRW records: {}", e.getMessage());
        }
        return false;
    }

    /*
     * Discover if there is any potential new HNFRT/HNFRW record
     * */
    private void performDiscovery(HNFTXNSection HNFTXNEntry) {
        log.debug("HNFTXNEntry: {}", HNFTXNEntry);
        if (HNFTXNEntry.getServiceName() != null && HNFTXNEntry.getServiceInstance() != null && HNFTXNEntry.getServiceHost() != null && HNFTXNEntry.getServiceType() != null) {
            boolean topologyExists = topologyExists(HNFTXNEntry);
            if (!topologyExists) {
                boolean topologyCreated = createHNFRTEvent(HNFTXNEntry);
                if (!topologyCreated) {
                    log.info("HNFRT Topology {} could not be created for:  ", HNFTXNEntry);
                }
            }
        } else {
            log.info("Topology could not be verified for: {} ", HNFTXNEntry.getServiceName());
        }
        if (HNFTXNEntry.getName() != null && HNFTXNEntry.getServiceName() != null && HNFTXNEntry.getDirection() != null) {
            boolean workLoadExists = workLoadExists(HNFTXNEntry);
            if (!workLoadExists) {
                boolean workLoadCreated = createHNFRWEvent(HNFTXNEntry);
                if (!workLoadCreated) {
                    log.info("HNFRW {} could not be created for:  ", HNFTXNEntry);
                }
            }
        } else {
            log.info("Workload could not be verified for: {} ", HNFTXNEntry.getServiceName());
        }
    }

}
