package com.appnomic.heal.etl.extractors.workloadkpi.hnftxn.pojo;


public class HNFTXN {
     BXNSection HNFBXN;
     SEGSection HNFSEG;
     HNFTXNSection HNFTXN;

     public BXNSection getHNFBXN() {
          return HNFBXN;
     }

     public void setHNFBXN(BXNSection HNFBXN) {
          this.HNFBXN = HNFBXN;
     }

     public SEGSection getHNFSEG() {
          return HNFSEG;
     }

     public void setHNFSEG(SEGSection HNFSEG) {
          this.HNFSEG = HNFSEG;
     }

     public HNFTXNSection getHNFTXN() {
          return HNFTXN;
     }

     public void setHNFTXN(HNFTXNSection HNFTXN) {
          this.HNFTXN = HNFTXN;
     }
}
