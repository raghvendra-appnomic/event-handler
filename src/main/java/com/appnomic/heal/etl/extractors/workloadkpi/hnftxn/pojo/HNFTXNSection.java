package com.appnomic.heal.etl.extractors.workloadkpi.hnftxn.pojo;


import java.util.List;

public class HNFTXNSection {
    String ServiceName;
    String ServiceInstance;
    String ServiceHost;
    String ServiceType;
    List<String> ServiceTags;
    String Name;
    String Direction;
    String PeerServiceName;
    String PeerServiceInstance;
    String PeerServiceHost;
    String ID;
    String Fingerprint;
    String StatusBool;
    String StatusCode;
    String StatusDesc;
    String TimestampStart;
    String TimestampEnd;
    float LatencyValue;
    int LatencyUnit;
    float AmountValue;
    String AmountCurrencyCode;
    String AmountCurrencyShort;
    String AmountCurrencyLong;

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public String getServiceInstance() {
        return ServiceInstance;
    }

    public void setServiceInstance(String serviceInstance) {
        ServiceInstance = serviceInstance;
    }

    public String getServiceHost() {
        return ServiceHost;
    }

    public void setServiceHost(String serviceHost) {
        ServiceHost = serviceHost;
    }

    public String getServiceType() {
        return ServiceType;
    }

    public void setServiceType(String serviceType) {
        ServiceType = serviceType;
    }

    public List<String> getServiceTags() {
        return ServiceTags;
    }

    public void setServiceTags(List<String> serviceTags) {
        ServiceTags = serviceTags;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDirection() {
        return Direction;
    }

    public void setDirection(String direction) {
        Direction = direction;
    }

    public String getPeerServiceName() {
        return PeerServiceName;
    }

    public void setPeerServiceName(String peerServiceName) {
        PeerServiceName = peerServiceName;
    }

    public String getPeerServiceInstance() {
        return PeerServiceInstance;
    }

    public void setPeerServiceInstance(String peerServiceInstance) {
        PeerServiceInstance = peerServiceInstance;
    }

    public String getPeerServiceHost() {
        return PeerServiceHost;
    }

    public void setPeerServiceHost(String peerServiceHost) {
        PeerServiceHost = peerServiceHost;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFingerprint() {
        return Fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        Fingerprint = fingerprint;
    }

    public String getStatusBool() {
        return StatusBool;
    }

    public void setStatusBool(String statusBool) {
        StatusBool = statusBool;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }

    public String getStatusDesc() {
        return StatusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        StatusDesc = statusDesc;
    }

    public String getTimestampStart() {
        return TimestampStart;
    }

    public void setTimestampStart(String timestampStart) {
        TimestampStart = timestampStart;
    }

    public String getTimestampEnd() {
        return TimestampEnd;
    }

    public void setTimestampEnd(String timestampEnd) {
        TimestampEnd = timestampEnd;
    }

    public float getLatencyValue() {
        return LatencyValue;
    }

    public void setLatencyValue(float latencyValue) {
        LatencyValue = latencyValue;
    }

    public int getLatencyUnit() {
        return LatencyUnit;
    }

    public void setLatencyUnit(int latencyUnit) {
        LatencyUnit = latencyUnit;
    }

    public float getAmountValue() {
        return AmountValue;
    }

    public void setAmountValue(float amountValue) {
        AmountValue = amountValue;
    }

    public String getAmountCurrencyCode() {
        return AmountCurrencyCode;
    }

    public void setAmountCurrencyCode(String amountCurrencyCode) {
        AmountCurrencyCode = amountCurrencyCode;
    }

    public String getAmountCurrencyShort() {
        return AmountCurrencyShort;
    }

    public void setAmountCurrencyShort(String amountCurrencyShort) {
        AmountCurrencyShort = amountCurrencyShort;
    }

    public String getAmountCurrencyLong() {
        return AmountCurrencyLong;
    }

    public void setAmountCurrencyLong(String amountCurrencyLong) {
        AmountCurrencyLong = amountCurrencyLong;
    }
}
