package com.appnomic.heal.etl.extractors.workloadkpi.hnftxnr;

import com.appnomic.heal.AdapterItem;
import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.elasticsearch.AbstractElasticExtractor;
import com.appnomic.heal.elasticsearch.ElasticConstants;
import com.appnomic.heal.elasticsearch.ElasticUtil;
import com.appnomic.heal.elasticsearch.SearchResults;
import com.appnomic.heal.etl.extractors.workloadkpi.hnftxnr.pojo.HNFTXNR;
import com.appnomic.heal.etl.utils.DateHelper;
import com.codahale.metrics.Meter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.search.*;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HNFTXNRExtractor extends AbstractElasticExtractor {

    final static Logger log = LoggerFactory.getLogger(HNFTXNRExtractor.class);
    private final Meter hnfrwExtraction = MasterCache.getInstance().getMetricsMeter("hnfrw-extraction");
    private SearchRequest searchRequest;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void initialize() throws Exception {
        super.initialize();
        if (this.isInitialized) {

            String indexPattern = this.parameters.getOrDefault(ElasticConstants.INDEX_KEY, ElasticConstants.HNFRW_DEFAULT_INDEX_PATTERN);
            Integer scrollSz = this.parameters.containsKey(ElasticConstants.SCROLLSIZE_KEY)
                    ? Integer.parseInt(this.parameters.get(ElasticConstants.SCROLLSIZE_KEY))
                    : ElasticConstants.ELASTIC_DEFAULT_SCROLL_SIZE;
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

            searchRequest = ElasticUtil.getSearchRequest(scrollSz, indexPattern);

            pingElasticSearch();

            this.isInitialized = true;
            log.info("{} initialized", this.className);
        } else {
            log.error("Base class not initialized!");
        }
    }

    @Override
    public List<AdapterItem> extract(Date from, Date to) {
        validateConnection();
        if (!this.isInitialized) {
            log.info("Initialization Error! Cannot execute extraction");
            return null;
        }

        log.info("Extraction from {} to {} by chain {} in Thread {}", DateHelper.date2stringConverter(from),
                DateHelper.date2stringConverter(to), this.currentChain.getChainId(), Thread.currentThread().getId());
        List<AdapterItem> results = new ArrayList<>();
        BoolQueryBuilder boolQueryBuilder = (BoolQueryBuilder) searchRequest.source().query();
        boolQueryBuilder.must().clear();
        boolQueryBuilder.must(QueryBuilders.rangeQuery(ElasticConstants.TIMESTAMP_KEY).gte(from).lte(to));

        log.debug("Request to Elasticsearch : {}", searchRequest);
        SearchResults searchResults = ElasticUtil.getSearchHit(elasticClient, searchRequest, null);

        do {
            for (SearchHit hit : searchResults.getSearchHits()) {
                AdapterItem item = new AdapterItem();
                String _source = hit.getSourceAsString();
                try {
                    HNFTXNR HNFTXNREntry = objectMapper.readValue(_source, new TypeReference<HNFTXNR>() {});
                    log.debug("HNFTXNREntry: {}", HNFTXNREntry);
                    item.setSourceItem(HNFTXNREntry);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                results.add(item);
            }
            if(searchResults.getScrollId()!=null) {
                searchResults = ElasticUtil.getSearchHit(elasticClient, searchRequest, searchResults.getScrollId());
            }
        }
        while (searchResults.getSearchHits() != null && searchResults.getSearchHits().length > 0);

        if(searchResults.getScrollId()!=null) {
            boolean succeeded = ElasticUtil.stopScrolling(elasticClient, searchResults.getScrollId());
            log.info("Scrolling cleared: {} Extracted {} potential transactions", succeeded, results.size());
        }

        hnfrwExtraction.mark();
        return results;
    }
}
