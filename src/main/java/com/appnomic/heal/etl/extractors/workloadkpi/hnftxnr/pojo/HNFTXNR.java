package com.appnomic.heal.etl.extractors.workloadkpi.hnftxnr.pojo;

import com.appnomic.heal.elasticsearch.ElasticConstants;
import com.appnomic.heal.etl.extractors.workloadkpi.hnftxn.pojo.HNFTXNSection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static java.util.UUID.randomUUID;

public class HNFTXNR {

    HNFTXNRSection HNFTXNR;

    public static Map<String, Object> createJson(HNFTXNSection txn) {
        Map<String, Object> HNFTXNRJson = new HashMap<>();

        HNFTXNRJson.put(ElasticConstants.TIMESTAMP_KEY, new Date());
        HNFTXNRJson.put("HNFTXNR.Name", txn.getName());
        HNFTXNRJson.put("HNFTXNR.Direction", txn.getDirection());
        HNFTXNRJson.put("HNFTXNR.ServiceName", txn.getServiceName());
        HNFTXNRJson.put("HNFTXNR.StatusCodeToBool", txn.getStatusBool());
        HNFTXNRJson.put("HNFTXNR.SlowLatencyValue", txn.getLatencyValue());
        HNFTXNRJson.put("HNFTXNR.SlowLatencyUnit", txn.getLatencyUnit());

        return HNFTXNRJson;
    }


    public HNFTXNRSection getHNFTXNR() {
        return HNFTXNR;
    }

    public void setHNFTXNR(HNFTXNRSection HNFTXNR) {
        this.HNFTXNR = HNFTXNR;
    }

    public static String createIndex() {
        String prefix = ElasticConstants.HNFRW_DEFAULT_INDEX_PATTERN.substring(0, ElasticConstants.HNFRW_DEFAULT_INDEX_PATTERN.length() - 2);
        return prefix + "-" + new Date().getTime();
    }

    public static String createId() {
        return randomUUID().toString();
    }
}
