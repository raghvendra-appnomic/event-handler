package com.appnomic.heal.etl.extractors.workloadkpi.hnftxnr.pojo;

public class HNFTXNRSection {
    String Name;
    String Direction;
    String ServiceName;
    String StatusCodeToBool;
    float SlowLatencyValue;
    int SlowLatencyUnit;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDirection() {
        return Direction;
    }

    public void setDirection(String direction) {
        Direction = direction;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public String getStatusCodeToBool() {
        return StatusCodeToBool;
    }

    public void setStatusCodeToBool(String statusCodeToBool) {
        StatusCodeToBool = statusCodeToBool;
    }

    public float getSlowLatencyValue() {
        return SlowLatencyValue;
    }

    public void setSlowLatencyValue(float slowLatencyValue) {
        SlowLatencyValue = slowLatencyValue;
    }

    public int getSlowLatencyUnit() {
        return SlowLatencyUnit;
    }

    public void setSlowLatencyUnit(int slowLatencyUnit) {
        SlowLatencyUnit = slowLatencyUnit;
    }
}
