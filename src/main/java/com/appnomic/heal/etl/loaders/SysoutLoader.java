package com.appnomic.heal.etl.loaders;

import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.etl.ILoader;
import com.appnomic.heal.AdapterItem;
import com.appnomic.heal.etl.AbstractChainableWorker;
import com.codahale.metrics.Meter;

public class SysoutLoader extends AbstractChainableWorker implements ILoader {
	private final Meter sysoutLoad = MasterCache.getInstance().getMetricsMeter("sysout-load");

	@Override
	public void initialize() throws Exception {
		super.initialize();
		this.className = SysoutLoader.class.getName();
		this.isInitialized = true;
	}

	@Override
	public void load(AdapterItem o) {
		System.out.println(o.getDestItem().toString());
		sysoutLoad.mark();
	}

}
