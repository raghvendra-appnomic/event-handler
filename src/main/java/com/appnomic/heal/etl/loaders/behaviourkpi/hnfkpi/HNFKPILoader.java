package com.appnomic.heal.etl.loaders.behaviourkpi.hnfkpi;

import com.appnomic.appsone.common.protbuf.KPIAgentMessageProtos.KPIAgentMessage;
import com.appnomic.heal.AdapterItem;
import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.etl.ILoader;
import com.appnomic.heal.grpc.AbstractGRPCConnector;
import com.codahale.metrics.Meter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HNFKPILoader extends AbstractGRPCConnector implements ILoader {

	final static Logger log = LoggerFactory.getLogger(HNFKPILoader.class);
	private final Meter kpiLoader = MasterCache.getInstance().getMetricsMeter("kpi-loader");

	@Override
	public void load(AdapterItem item) {

		if (!this.isInitialized) {
			attemptReconnection();
		}
		try {
			KPIAgentMessage request = (KPIAgentMessage) item.getDestItem();
			this.asyncStub.publishKPIAgentMessage(request, observer);
			// log.debug("Received response {} when pushing LA KPI: {}", responseCode,
			kpiLoader.mark();
			if (this.retryAttempts > 0) {
				this.retryAttempts = 0;
			}
		} catch (Exception e) {
			log.error("Exception when loading item {} : {}", item.getDestItem(), e);
			if (attemptReconnection() == true) {
				log.debug("Connection seems to be restored. Attempting to push item {}", item.getDestItem());
				load(item);
			} else {
				log.error("Unable to push Items due to failure in connection with endpoint");
			}
		}
	}
}
