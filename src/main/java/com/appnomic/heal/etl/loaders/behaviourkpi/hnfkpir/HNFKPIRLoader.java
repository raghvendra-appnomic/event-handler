package com.appnomic.heal.etl.loaders.behaviourkpi.hnfkpir;

import com.appnomic.appsone.eventhandler.pojo.request.CategoryPojo;
import com.appnomic.appsone.eventhandler.pojo.request.GroupKPIPojo;
import com.appnomic.appsone.eventhandler.pojo.response.TokenPojo;
import com.appnomic.heal.AdapterItem;
import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.etl.AbstractChainableWorker;
import com.appnomic.heal.etl.extractors.behaviourkpi.hnfkpir.pojo.HNFKPIR;
import com.appnomic.heal.etl.utils.APIUtil;
import com.appnomic.heal.etl.ILoader;
import com.codahale.metrics.Meter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class HNFKPIRLoader extends AbstractChainableWorker implements ILoader {
	final static Logger log = LoggerFactory.getLogger(HNFKPIRLoader.class);
	private final Meter hnfrmLoad = MasterCache.getInstance().getMetricsMeter("hnfrm-loader");

	@Override
	public void load(AdapterItem item) {
		if (this.isInitialized) {
			log.info("HNFRMLoader");
			HNFKPIR HNFRMEntry = (HNFKPIR) item.getDestItem();
			TokenPojo token = APIUtil.getToken();
			log.info("Access Token : {} ", token.getAccess_token());

			String accountIdentifier = MasterCache.getInstance().getAccountIdentifier();

			GroupKPIPojo groupKPIPojo = GroupKPIPojo.builder()
					.groupKpiName("GROUP_KPI_NAME")
					.groupKpiIdentifier(UUID.randomUUID().toString())
					.description("NOT_KNOWN")
					.kpiType("NOT_KNOWN").discovery(0).build();
			boolean groupKpiAdded = APIUtil.addGroupKPI(token, accountIdentifier, groupKPIPojo);


			CategoryPojo categoryPojo = CategoryPojo.builder()
					.name(UUID.randomUUID().toString())
					.identifier(UUID.randomUUID().toString())
					.isWorkLoad(false)
					.build();

			boolean categoryAdded = APIUtil.addCategory(token, accountIdentifier, categoryPojo);
			hnfrmLoad.mark();
		}
	}
}
