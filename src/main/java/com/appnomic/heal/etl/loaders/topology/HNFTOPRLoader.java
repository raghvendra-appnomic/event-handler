package com.appnomic.heal.etl.loaders.topology;

import com.appnomic.appsone.eventhandler.api.Instances;
import com.appnomic.appsone.eventhandler.pojo.response.*;
import com.appnomic.heal.AdapterItem;
import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.etl.AbstractChainableWorker;
import com.appnomic.heal.etl.extractors.topology.pojo.Topology;
import com.appnomic.heal.etl.utils.APIUtil;
import com.appnomic.heal.etl.ILoader;
import com.codahale.metrics.Meter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

public class HNFTOPRLoader extends AbstractChainableWorker implements ILoader {
	final static Logger log = LoggerFactory.getLogger(HNFTOPRLoader.class);
	private final Meter topologyLoader = MasterCache.getInstance().getMetricsMeter("topology-loader");

	@Override
	public void load(AdapterItem item) {
		if (this.isInitialized) {
			log.info("TopologyLoader");
			Topology topologicalEntry = (Topology) item.getDestItem();
			String accountIdentifier = MasterCache.getInstance().getAccountIdentifier();
			ServiceResponsePojo targetServiceFound = MasterCache.getInstance().getServiceByName(accountIdentifier, topologicalEntry.getCompName());
			if (topologicalEntry.isCompIsService()) {
				if (targetServiceFound != null) {
					log.info("Service already Exists! : {} ", topologicalEntry.getCompName());
				} else {
					addService(topologicalEntry);
				}
			} else {
				if (targetServiceFound != null) {
					boolean exists = instanceExists(targetServiceFound, topologicalEntry.getCompInstance());
					if (exists) {
						log.info("Instance already Exists! : {} ", topologicalEntry.getCompInstance());
					} else {
						boolean instanceAdded = addInstance(targetServiceFound, topologicalEntry, accountIdentifier);
						log.info("New instance {} - created : {} ", instanceAdded, topologicalEntry.getCompInstance());
					}
				}
			}
			topologyLoader.mark();
		}
	}

	private boolean addService(Topology topologicalRecord){
		log.info("Adding new service : {} ", topologicalRecord.getCompName());
		TokenPojo token = APIUtil.getToken();
		log.info("Access Token : {} ", token.getAccess_token());
		boolean created = APIUtil.addService(topologicalRecord, token);
		log.info("New service {} - created : {} ", created, topologicalRecord.getCompName());
		return created;
	}

	private boolean instanceExists(ServiceResponsePojo service, String instanceName) {
		ArrayList<String> compInstanceList = (ArrayList<String>) service.getComponentInstance().get("name");
		return compInstanceList
				.stream()
				.filter(instance -> instance.equals(instanceName))
				.findFirst()
				.isPresent();
	}

	private boolean addInstance(ServiceResponsePojo service, Topology topologicalRecord, String accountIdentifier) {
		TokenPojo token = APIUtil.getToken();
		Instances instances = new Instances();

		List<String> serviceIdentifiers = new ArrayList<>();
		serviceIdentifiers.add(service.getIdentifier());

		List<ComponentInstanceResponsePojo> componentInstances = new ArrayList<>();

		ComponentInstanceResponsePojo compInstance = ComponentInstanceResponsePojo.builder()
				.name(topologicalRecord.getCompInstance()).identifier(UUID.randomUUID().toString())
				.componentName("NOT_KNOWN").componentVersion("NOT_KNOWN")
				.serviceIdentifiers(serviceIdentifiers).build();

		componentInstances.add(compInstance);

		boolean instanceAdded = instances.addInstance(accountIdentifier, componentInstances, token);
		return instanceAdded;
	}
}
