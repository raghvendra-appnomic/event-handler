package com.appnomic.heal.etl.loaders.workloadkpi.hnftxn;

import com.appnomic.appsone.common.protbuf.PSAgentMessageProtos.PSAgentMessage;
import com.appnomic.heal.AdapterItem;
import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.etl.ILoader;
import com.appnomic.heal.grpc.AbstractGRPCConnector;
import com.codahale.metrics.Meter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HNFTXNLoader extends AbstractGRPCConnector implements ILoader {
	final static Logger log = LoggerFactory.getLogger(HNFTXNLoader.class);
	private final Meter transactionLoader = MasterCache.getInstance().getMetricsMeter("transaction-loader");

	@Override
	public void load(AdapterItem item) {
		if (this.isInitialized) {
			try {
//				long start = System.currentTimeMillis();
				PSAgentMessage request = (PSAgentMessage) item.getDestItem();
//				RPCServiceProtos.ResponseCode responseCode = blockingStub.publishTransactionMessage(request);
				this.asyncStub.publishTransactionMessage(request, observer);
//				log.debug("Received response {} when pushing LTM Message: {}", responseCode, item.getDestItem());
				transactionLoader.mark();
				if (this.retryAttempts > 0) {
					this.retryAttempts = 0;
				}
//				log.debug("Item loaded in {} ms with Response {}", System.currentTimeMillis() - start, responseCode);
			} catch (Exception e) {
				log.error("Exception when loading item : {}", e);
				if (attemptReconnection() == true) {
					load(item);
				} else {
					log.error("Unable to push Items due to failure in connection with endpoint");
				}
			}
		} else {
			log.error("Unable to load item since Loader is not initialized");
			log.debug("Unable to load item {} since Loader is not initialized", item);
		}
	}

}
