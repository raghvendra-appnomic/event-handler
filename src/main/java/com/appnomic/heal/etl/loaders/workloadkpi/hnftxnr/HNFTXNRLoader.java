package com.appnomic.heal.etl.loaders.workloadkpi.hnftxnr;

import com.appnomic.appsone.eventhandler.pojo.request.KPIPojo;
import com.appnomic.appsone.eventhandler.pojo.response.KpiNamesPojo;
import com.appnomic.appsone.eventhandler.pojo.response.TokenPojo;
import com.appnomic.heal.AdapterItem;
import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.etl.AbstractChainableWorker;
import com.appnomic.heal.etl.extractors.workloadkpi.hnftxnr.pojo.HNFTXNR;
import com.appnomic.heal.etl.utils.APIUtil;
import com.appnomic.heal.etl.ILoader;
import com.codahale.metrics.Meter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.UUID;

public class HNFTXNRLoader extends AbstractChainableWorker implements ILoader {
	final static Logger log = LoggerFactory.getLogger(HNFTXNRLoader.class);
	private final Meter hnfrwLoad = MasterCache.getInstance().getMetricsMeter("hnfrw-loader");

	@Override
	public void load(AdapterItem item) {
		if (this.isInitialized) {
			log.info("HNFRWLoader");
			HNFTXNR HNFTXNREntry = (HNFTXNR) item.getDestItem();
			String accountIdentifier = MasterCache.getInstance().getAccountIdentifier();
			TokenPojo token  = APIUtil.getToken();
			log.info("Access Token : {} ",token.getAccess_token());

			List<KpiNamesPojo> kpiList = APIUtil.getKPIs(token, accountIdentifier, 0, 0, 0 );

			KPIPojo kpiPojo = KPIPojo.builder()
					.kpiName(HNFTXNREntry.getHNFTXNR().getName()).kpiIdentifier(UUID.randomUUID().toString())
					.description("Node Current CPU usage").groupKpiIdentifier("None")
					.dataType("Float").kpiType("Core").clusterOperation("None")
					.rollupOperation("None").instanceAggregation("None")
					.clusterAggregation("None").collectionIntervalSeconds(60)
					.measureUnits("%").componentName("Node").componentType("Host")
					.componentVersion("1.12.0").categoryName("CPU")
					.enableAnalytics(1).build();

			boolean kpiAdded  = APIUtil.addKPI(token, accountIdentifier, kpiPojo);
			hnfrwLoad.mark();
		}
	}
}
