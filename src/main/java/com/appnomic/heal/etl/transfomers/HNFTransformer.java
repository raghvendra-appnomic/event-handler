package com.appnomic.heal.etl.transfomers;

import com.appnomic.heal.AdapterItem;
import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.etl.AbstractChainableWorker;
import com.appnomic.heal.etl.ITransformer;
import com.codahale.metrics.Meter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HNFTransformer extends AbstractChainableWorker implements ITransformer {
    final static Logger log = LoggerFactory.getLogger(HNFTransformer.class);
    private final Meter hnfTransform = MasterCache.getInstance().getMetricsMeter("hnf-transform");

    @Override
    public void transform(AdapterItem item) {
        item.setDestItem(item.getSourceItem());
        log.info("HNFTransformer : empty transform");
        hnfTransform.mark();
    }

}
