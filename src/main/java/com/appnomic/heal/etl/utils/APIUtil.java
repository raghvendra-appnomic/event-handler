package com.appnomic.heal.etl.utils;

import com.appnomic.appsone.eventhandler.pojo.request.CategoryPojo;
import com.appnomic.appsone.eventhandler.pojo.request.GroupKPIPojo;
import com.appnomic.appsone.eventhandler.pojo.request.KPIPojo;
import com.appnomic.appsone.eventhandler.pojo.request.ServiceRequestPojo;
import com.appnomic.appsone.eventhandler.pojo.response.*;
import com.appnomic.appsone.eventhandler.rxokhttp.RxHttpClient;
import com.appnomic.heal.cache.MasterCache;
import com.appnomic.heal.etl.extractors.topology.pojo.Topology;
import io.reactivex.rxjava3.core.Observable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class APIUtil {

    public static TokenPojo getToken() {
        RxHttpClient rxHttpClient = RxHttpClient.newRxClient("");
        Observable<TokenPojo> observable = rxHttpClient.getToken();
        return observable.blockingLast();
    }

    public static boolean addService(Topology topologicalRecord, TokenPojo token) {
        String accountIdentifier = MasterCache.getInstance().getAccountIdentifier();

        ServiceRequestPojo servicePojo = ServiceRequestPojo.builder()
                .name(topologicalRecord.getCompName()).identifier(UUID.randomUUID().toString())
                .build();

        List<ServiceRequestPojo> serviceList = new ArrayList<>();

        serviceList.add(servicePojo);

        RxHttpClient rxHttpClient = RxHttpClient.newRxClient("");
        Observable<Boolean> observable = rxHttpClient.addService(accountIdentifier, serviceList, token);
        return observable.blockingLast();
    }

    public static List<ServiceResponsePojo> getServices(String accountIdentifier, TokenPojo token) {
        RxHttpClient rxHttpClient = RxHttpClient.newRxClient("");
        Observable<List<ServiceResponsePojo>> observable = rxHttpClient.getServices(token, accountIdentifier);
        return observable.blockingLast();
    }

    public static List<ApplicationResponsePojo> getApplications(String accountIdentifier, TokenPojo token) {
        RxHttpClient rxHttpClient = RxHttpClient.newRxClient("");
        Observable<List<ApplicationResponsePojo>> observable = rxHttpClient.getApplications(token, accountIdentifier);
        return observable.blockingLast();
    }

    public static List<AccountConfiguration> getTxnConfigData() {
        RxHttpClient rxHttpClient = RxHttpClient.newRxClient("");
        Observable<List<AccountConfiguration>> observable = rxHttpClient.getTxnConfigData();
        return observable.blockingLast();
    }

    public static List<AccountConfiguration> getAgentConfigData(String agentIdentifier) {
        RxHttpClient rxHttpClient = RxHttpClient.newRxClient("");
        Observable<List<AccountConfiguration>> observable = rxHttpClient.getAgentConfigData(agentIdentifier);
        return observable.blockingLast();
    }

    public static List<ComponentInstanceResponsePojo> getInstances(TokenPojo token, String accountIdentifier, int serviceId) {
        RxHttpClient rxHttpClient = RxHttpClient.newRxClient("");
        Observable<List<ComponentInstanceResponsePojo>> observable = rxHttpClient.getInstances(token, accountIdentifier, serviceId);
        return observable.blockingLast();
    }

    public static List<KpiNamesPojo> getKPIs(TokenPojo token, String accountIdentifier, int serviceId, int instanceId, int categoryId) {
        RxHttpClient rxHttpClient = RxHttpClient.newRxClient("");
        Observable<List<KpiNamesPojo>> observable = rxHttpClient.getKPIs(token, accountIdentifier, serviceId, instanceId, categoryId);
        return observable.blockingLast();
    }

    public static boolean addCategory(TokenPojo token, String accountIdentifier, CategoryPojo categoryPojo) {
        RxHttpClient rxHttpClient = RxHttpClient.newRxClient("");
        Observable<Boolean> observable = rxHttpClient.addCategory(accountIdentifier, categoryPojo, token);
        return observable.blockingLast();
    }

    public static boolean addKPI(TokenPojo token, String accountIdentifier, KPIPojo kpiPojo) {
        RxHttpClient rxHttpClient = RxHttpClient.newRxClient("");
        Observable<Boolean> observable = rxHttpClient.addKPI(accountIdentifier, kpiPojo, token);
        return observable.blockingLast();
    }

    public static boolean addGroupKPI(TokenPojo token, String accountIdentifier, GroupKPIPojo groupKPIPojo) {
        RxHttpClient rxHttpClient = RxHttpClient.newRxClient("");
        Observable<Boolean> observable = rxHttpClient.addGroupKPI(accountIdentifier, groupKPIPojo, token);
        return observable.blockingLast();
    }
}