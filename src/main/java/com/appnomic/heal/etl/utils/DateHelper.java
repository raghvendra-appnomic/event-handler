package com.appnomic.heal.etl.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateHelper {
	private static final String defaultDateFormat = "yyyy-MM-dd HH:mm:ss";
	private static final String defaultDateFormatTxns = "yyyy-MM-dd HH:mm:ss.SSS";
	private static final String defaultElkDateFormat = "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'";
	private static final Logger logger = LoggerFactory.getLogger(DateHelper.class);

	public static String date2stringConverter(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(defaultDateFormat);
		return dateFormat.format(date);
	}

	public static Date string2dateConverter(String dateAsString) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(defaultDateFormat);
		try {
			return dateFormat.parse(dateAsString);
		} catch (ParseException e) {
			logger.error("Error Parsing {} into date : {}", dateAsString, e);
			return null;
		}
	}

	public static String date2GMTString(Date date) {
		DateFormat gmtFormat = new SimpleDateFormat(defaultDateFormat);
		TimeZone gmtTime = TimeZone.getTimeZone("GMT");
		gmtFormat.setTimeZone(gmtTime);
		return gmtFormat.format(date);
	}

	public static String date2GMTString4Txns(Date date) {
		DateFormat gmtFormat = new SimpleDateFormat(defaultDateFormatTxns);
		TimeZone gmtTime = TimeZone.getTimeZone("GMT");
		gmtFormat.setTimeZone(gmtTime);
		return gmtFormat.format(date);
	}

	public static Date string2ElkDateConverter(String dateAsString) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(defaultElkDateFormat);
		try {
			return dateFormat.parse(dateAsString);
		} catch (ParseException e) {
			logger.error("Error Parsing {} into date : {}", dateAsString, e);
			return null;
		}
	}
}
