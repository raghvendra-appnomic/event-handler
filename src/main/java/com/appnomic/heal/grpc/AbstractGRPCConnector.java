package com.appnomic.heal.grpc;

import com.appnomic.appsone.common.protbuf.MessagePublisherGrpc;
import com.appnomic.appsone.common.protbuf.RPCServiceProtos.ResponseCode;
import com.appnomic.heal.constants.Constants;
import com.appnomic.heal.etl.AbstractChainableWorker;
import io.grpc.ManagedChannel;
import io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.NettyChannelBuilder;
import io.grpc.stub.StreamObserver;
import io.netty.handler.ssl.OpenSsl;
import io.netty.handler.ssl.SslProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public abstract class AbstractGRPCConnector extends AbstractChainableWorker {

	protected MessagePublisherGrpc.MessagePublisherStub asyncStub;
	final static Logger log = LoggerFactory.getLogger(AbstractGRPCConnector.class);
	protected ManagedChannel channel = null;
	protected boolean sslEnabled = false;
	protected String certificateFile;
	protected String grpcServer = "";
	protected Integer grpcPort = -1;
	protected Integer maxRetryAttempts;
	protected Integer retryAttempts = 0;
	protected Integer retryInterval;
	protected StreamObserver<ResponseCode> observer;

	@Override
	public void initialize() throws Exception {
		super.initialize();

		try {
			log.info("Initialization Parameters for Heal Connector: {}", this.parameters);
			if (this.parameters != null) {
				if (this.parameters.containsKey(Constants.SSL_ENABLED)) {
					this.sslEnabled = Boolean.valueOf(this.parameters.get(Constants.SSL_ENABLED));
				}
				if (this.parameters.containsKey(Constants.SSL_CERT)) {
					this.certificateFile = this.parameters.get(Constants.SSL_CERT);
				}
				this.grpcServer = this.parameters.get(Constants.GRPC_ENDPOINT);
				this.grpcPort = Integer.valueOf(this.parameters.get(Constants.GRPC_PORT));
				this.maxRetryAttempts = (this.parameters.containsKey(Constants.RETRY_ATTEMPTS)
						? Integer.valueOf(this.parameters.get(Constants.RETRY_ATTEMPTS))
						: 5);
				this.retryInterval = 1000 * (this.parameters.containsKey(Constants.RETRY_INTERVAL)
						?  Integer.valueOf(this.parameters.get(Constants.RETRY_INTERVAL))
						: 5);
			}

			if (this.sslEnabled) {

				log.debug("OpenSsl.isAvailable(): {}" + OpenSsl.isAvailable());
				if (OpenSsl.isAvailable()) {
					log.debug("OpenSsl version: {}", OpenSsl.versionString());
				}
				File certFile = new File(this.certificateFile);
				log.debug("Certificate file path '" + this.certificateFile + "', File Exists? :" + certFile.exists());
				io.netty.handler.ssl.SslContext sslContext = null;
				if (certFile.exists()) {
					sslContext = GrpcSslContexts.forClient().sslProvider(SslProvider.OPENSSL).trustManager(certFile)
							.protocols("TLSv1.2").build();
				} else {
					sslContext = GrpcSslContexts.forClient().sslProvider(SslProvider.OPENSSL).build();
				}
			
				
				
				channel = NettyChannelBuilder.forAddress((String) grpcServer, grpcPort).sslContext(sslContext).build();
			} else {
				channel = NettyChannelBuilder.forAddress(grpcServer, grpcPort).build();
			}

			observer = new StreamObserver<ResponseCode>() {

				@Override
				public void onNext(ResponseCode value) {
					log.info("Sent KPI to Heal with response code : {}", value);
				}				

				@Override
				public void onError(Throwable t) {
					log.error("Error for response: {}", t);
				}

				@Override
				public void onCompleted() {
					log.info("Sending KPIs complete");
				}
			};

			this.asyncStub = MessagePublisherGrpc.newStub(channel);
			this.asyncStub.send(null, observer);
			this.isInitialized = true;
			log.info("{} Intialized!", this.className);
		} catch (Exception e) {
			log.error("Exception during initialization of {} with parameters {} : {}", this.getClass().getName(),
					this.parameters, e);
			this.isInitialized = false;
		}
	}

	protected Boolean attemptReconnection() {
		try {
			if (this.maxRetryAttempts == -1 || this.retryAttempts < this.maxRetryAttempts) {
				log.debug("Attempting to re-initialize {}", this.className);
				initialize();
			} else {
				log.error("Despite {} attempts, connection could not be established with endpoint", this.retryAttempts,
						this.grpcServer, this.grpcPort);
				return false;
			}

			if (this.isInitialized == false) {
				retryAttempts++;
				try {
					Thread.sleep(this.retryInterval);
				} catch (InterruptedException e) {
					log.error("Exception when attempting to Sleep : {}", e);
				}
				return attemptReconnection();
			}
		} catch (Exception ex) {
			log.info("Reconnection attempt failed to endpoint {}:{} : {}", this.grpcServer, this.grpcPort, ex);
		}
		return isInitialized;
	}
}
