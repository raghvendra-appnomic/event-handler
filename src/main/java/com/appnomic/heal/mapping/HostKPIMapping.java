package com.appnomic.heal.mapping;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HostKPIMapping {
    String kpiNameInHeal;
    String kpiNameFromClientData;
    String kpiCategory;
}
