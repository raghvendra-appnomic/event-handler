package com.appnomic.heal.mapping;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InstanceKPIMapping {
    String componentType;
    String kpiNameInHeal;
    String kpiValueField;
    String kpiNameFromClientData;
}
