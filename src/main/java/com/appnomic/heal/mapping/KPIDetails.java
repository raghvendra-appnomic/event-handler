package com.appnomic.heal.mapping;

import lombok.Data;

@Data
public class KPIDetails {
    int kpiId;
    String kpiName;
    String kpiType;
    boolean isGroup;
    int collectionInterval;
}
