SET SQL_SAFE_UPDATES = 0;
drop database if exists dataadapter;

create user if not exists 'appsone'@'%' IDENTIFIED with mysql_native_password BY 'password';
alter user 'appsone'@'%' IDENTIFIED with mysql_native_password BY 'password';

create database dataadapter;

grant all privileges on dataadapter.* to 'appsone'@'%';

drop table if exists `dataadapter`.`a1_adapter_state`;
create table `dataadapter`.`a1_adapter_state`(
    `name` varchar(48) not null primary key,
    `value` varchar (48)
)ENGINE=InnoDB AUTO_INCREMENT=6878 DEFAULT CHARSET=latin1;

drop table if exists `dataadapter`.`a1_logscan_endpt`;
create table `dataadapter`.`a1_logscan_endpt`
(
	id int not null auto_increment primary key,
	`host` varchar(48) not null,
    port int not null default 9200,
    `protocol` varchar(8) not null default 'http'
)ENGINE=InnoDB AUTO_INCREMENT=6878 DEFAULT CHARSET=latin1;

INSERT INTO `dataadapter`.`a1_logscan_endpt` (`host`) VALUES ('192.168.0.105');